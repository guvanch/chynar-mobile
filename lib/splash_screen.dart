import 'package:chynar_mobile/app_container.dart';
import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/models/user_model.dart';
import 'package:chynar_mobile/models/user_profile_model.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final storage = new FlutterSecureStorage();

  Widget _render(BuildContext context, ViewModel vm) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Image.asset(
              'assets/images/chynar.png',
              height: 100,
              width: screenWidth * 0.7,
            ),
          ),
          Container(
            padding: EdgeInsets.all(40),
            child: CircularProgressIndicator(
              color: AppTheme.primary,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (ViewModel vm) async {
        try {
          String id = await storage.read(key: 'id');
          if (id != null) {
            int userId = int.parse(id);
            List<UserAddressModel> userAddresses =
                await UserService.getUserAddress(userId);
            await vm.setUserAddresses(userAddresses);
            if (userAddresses != null && userAddresses.length > 0) {
              UserProfileModel userProfile = userAddresses[0].userProfile;
              UserModel user = new UserModel();
              user.id = userProfile.id;
              user.name = userProfile.firstname;
              user.typeId = userProfile.userTypeId;
              await vm.fetchFavorites(user.id);

              await vm.setUser(user);
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => AppContainer()));
            }
          } else {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => AppContainer()));
          }
        } catch (e) {
          print(e.toString());
        }
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
