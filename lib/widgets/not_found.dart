import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class NotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
        distinct: false,
        converter: (store) => ViewModel.fromStore(store),
        builder: (BuildContext context, ViewModel vm) {
          return Container(
              height: 80,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.sentiment_dissatisfied),
                  Text(vm.getLangValue('not_found')),
                ],
              ));
        });
  }
}
