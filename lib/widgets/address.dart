import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/profile/edit_address_screen.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Address extends StatefulWidget {
  final UserAddressModel address;
  final Function onClick;

  const Address({Key key, this.address, this.onClick}) : super(key: key);

  @override
  _AddressState createState() => _AddressState();
}

class _AddressState extends State<Address> {
  _onDelete(vm) async {
    try {
      var res = await UserService.deleteAddress(widget.address.id);
      EasyLoading.showSuccess(res['msg']);
      await vm.fetchUserAddresses();
      Navigator.pop(context);
    } catch (e) {
      EasyLoading.showError(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    UserAddressModel _address = widget.address;
    return StoreConnector<AppState, ViewModel>(
        distinct: false,
        converter: (store) => ViewModel.fromStore(store),
        builder: (BuildContext context, ViewModel vm) {
          return InkWell(
            onTap: widget.onClick,
            child: Container(
                margin: EdgeInsets.all(4),
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: Offset(0, 2)),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.home_outlined,
                                color: AppTheme.primary,
                                size: 24,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text(
                                  _address.recAddress,
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(2),
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                decoration: BoxDecoration(
                                    color: AppTheme.secondary,
                                    borderRadius: BorderRadius.circular(4)),
                                child: InkWell(
                                    splashColor:
                                        AppTheme.secondary.withOpacity(0.5),
                                    onTap: () => _onDelete(vm),
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.white,
                                      size: 18,
                                    )),
                              ),
                              Container(
                                padding: EdgeInsets.all(2),
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                decoration: BoxDecoration(
                                    color: AppTheme.secondary,
                                    borderRadius: BorderRadius.circular(4)),
                                child: InkWell(
                                  splashColor:
                                      AppTheme.secondary.withOpacity(0.5),
                                  onTap: () {
                                    _toEditAddress();
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.account_circle_outlined,
                            color: AppTheme.primary,
                            size: 24,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              _address.recName,
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.phone,
                            color: AppTheme.primary,
                            size: 22,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              _address.recNumber,
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // InkWell(
                    //   splashColor: AppTheme.secondary.withOpacity(0.5),
                    //   onTap: () {
                    //     setState(() {
                    //       _address.isMain = !_address.isMain;
                    //     });
                    //   },
                    //   child: Padding(
                    //     padding: const EdgeInsets.all(8.0),
                    //     child: Row(
                    //       crossAxisAlignment: CrossAxisAlignment.center,
                    //       children: [
                    //         Icon(
                    //           _address.isMain
                    //               ? Icons.check_box_outlined
                    //               : Icons.check_box_outline_blank,
                    //           color: _address.isMain
                    //               ? AppTheme.secondary
                    //               : Colors.grey,
                    //           size: 24,
                    //         ),
                    //         Padding(
                    //           padding: const EdgeInsets.only(left: 8.0),
                    //           child: Text(
                    //             vm.getLangValue('main_address'),
                    //             overflow: TextOverflow.fade,
                    //             style: TextStyle(
                    //                 fontSize: 14, color: AppTheme.secondary),
                    //           ),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                  ],
                )),
          );
        });
  }

  void _toEditAddress() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditAddressScreen(
                  address: widget.address,
                )));
  }
}
