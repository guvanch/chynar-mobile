import 'package:chynar_mobile/app_theme.dart';
import 'package:flutter/material.dart';

class ProfileAction extends StatefulWidget {
  final String title;
  final IconData icon;
  final Function onClick;

  const ProfileAction({
    Key key,
    @required this.title,
    @required this.icon,
    @required this.onClick,
  }) : super(key: key);

  @override
  _ProfileActionState createState() => _ProfileActionState();
}

class _ProfileActionState extends State<ProfileAction> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onClick,
      splashColor: AppTheme.primary.withOpacity(0.5),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              widget.icon,
              color: AppTheme.primary,
              size: 30,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                widget.title,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
      ),
    );
  }
}
