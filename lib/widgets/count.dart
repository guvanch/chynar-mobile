import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Count extends StatefulWidget {
  final Function increment;
  final Function decrement;
  final Function stopEditing;
  final bool hideable;
  final ProductModel product;
  final double amount;

  const Count(
      {Key key,
      this.increment,
      this.decrement,
      this.stopEditing,
      this.product,
      this.hideable,
      this.amount})
      : super(key: key);

  @override
  _CountState createState() => _CountState();
}

class _CountState extends State<Count> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
        distinct: false,
        converter: (store) => ViewModel.fromStore(store),
        builder: (BuildContext context, ViewModel vm) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  _decrement();
                },
                child: Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: AppTheme.secondary),
                  child: Icon(
                    Icons.remove,
                    size: 18,
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 2),
                  color: Colors.white,
                  child: Text(
                    "${widget.amount != null ? widget.amount : widget.product.amountInCart} ${widget.product.unit != null ? widget.product.unit.name : ''}",
                    style: TextStyle(fontSize: 16),
                  )),
              InkWell(
                onTap: () {
                  widget.increment();
                  if (widget.hideable) {
                    widget.stopEditing(1500);
                  }
                },
                child: Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: AppTheme.secondary),
                  child: Icon(
                    Icons.add,
                    size: 18,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          );
        });
  }

  void _decrement() {
    if (widget.amount != null) {
      if (widget.product.unitId == 12) {
        if (!((widget.amount - 1) < 1)) {
          widget.decrement();
        }
      } else {
        if (!((widget.amount - 0.5) < 0.5)) {
          widget.decrement();
        }
      }
    } else {
      if (widget.product.unitId == 12) {
        if (!((widget.product.amountInCart - 1) < 1)) {
          widget.decrement();
        }
      } else {
        if (!((widget.product.amountInCart - 0.5) < 0.5)) {
          widget.decrement();
        }
      }
    }

    if (widget.hideable) {
      widget.stopEditing(1500);
    }
  }
}
