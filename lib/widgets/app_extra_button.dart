import 'package:chynar_mobile/app_theme.dart';
import 'package:flutter/material.dart';

class AppExtraButton extends StatefulWidget {
  final String title;
  final Function onClick;
  AppExtraButton({Key key, this.title, this.onClick});

  @override
  _AppExtraButtonState createState() => _AppExtraButtonState();
}

class _AppExtraButtonState extends State<AppExtraButton> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: widget.onClick,
        style: TextButton.styleFrom(
          minimumSize: Size(double.infinity, 36),
          primary: AppTheme.secondary,
          side: BorderSide(color: AppTheme.secondary, width: 1.5),
        ),
        child: Text(
          widget.title,
          style: TextStyle(color: Colors.black, fontSize: 18),
        ));
  }
}
