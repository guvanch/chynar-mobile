import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Product extends StatefulWidget {
  final ProductModel product;

  const Product({Key key, this.product}) : super(key: key);

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  bool _editing = false;

  @override
  Widget build(BuildContext context) {
    var product = widget.product;
    String fileUrl = Constants.fileServerUrl;
    return StoreConnector<AppState, ViewModel>(
        distinct: false,
        converter: (store) => ViewModel.fromStore(store),
        builder: (BuildContext context, ViewModel vm) {
          return Stack(
            children: [
              Opacity(
                opacity: _editing ? 0.3 : 1.0,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white),
                  padding:
                      const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  child: IntrinsicHeight(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              flex: 4,
                              child: Text(
                                vm.lang == 'en'
                                    ? product.nameEn
                                    : vm.lang == 'ru'
                                        ? product.nameRu
                                        : product.nameTm,
                                overflow: TextOverflow.fade,
                                softWrap: true,
                                style: TextStyle(
                                  height: 1.5,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            Flexible(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  if (_liked(vm, product)) {
                                    vm.removeFromFavorites(product);
                                  } else {
                                    vm.addToFavorites(product);
                                  }
                                },
                                child: Icon(
                                  _liked(vm, product)
                                      ? Icons.favorite_outlined
                                      : Icons.favorite_border_outlined,
                                  size: 20,
                                  color: Colors.red,
                                ),
                              ),
                            )
                          ],
                        ),
                        Flexible(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 50,
                                height: 18,
                                decoration: BoxDecoration(
                                  color: product.isNew == true
                                      ? Colors.red
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(14.0),
                                ),
                                child: Center(
                                  child: Text(
                                    vm.getLangValue('new'),
                                    softWrap: true,
                                    maxLines: 2,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                        height: 1.5,
                                        fontSize: 10,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Center(
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(6.0),
                                child: FadeInImage(
                                  image: NetworkImage(
                                      "$fileUrl${widget.product.image}"),
                                  placeholder: AssetImage(
                                      "assets/images/transparent.png"),
                                  fit: BoxFit.contain,
                                  height: 80,
                                  width: 80,
                                  imageErrorBuilder: (BuildContext context,
                                      Object exception, StackTrace stackTrace) {
                                    return Container(
                                      width: 50,
                                      height: 50,
                                    );
                                  },
                                )),
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${product.price} TMT/${product.unit.name}',
                                style: TextStyle(fontSize: 14),
                              ),
                              Container(
                                padding: const EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    color: AppTheme.secondary),
                                child: Icon(
                                  Icons.shopping_cart_outlined,
                                  size: 20,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  bool _liked(ViewModel vm, ProductModel pro) {
    bool isExist = vm.favorites.any((element) => element.id == pro.id);
    return isExist;
  }
}
