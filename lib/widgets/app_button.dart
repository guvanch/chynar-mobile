import 'package:chynar_mobile/app_theme.dart';
import 'package:flutter/material.dart';

class AppButton extends StatefulWidget {
  final String title;
  final Function onClick;
  AppButton({Key key, this.title, this.onClick});

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: widget.onClick,
        style: ButtonStyle(
          overlayColor: MaterialStateColor.resolveWith(
              (states) => Colors.white.withOpacity(0.4)),
          minimumSize: MaterialStateProperty.resolveWith(
              (states) => Size(double.infinity, 36)),
          backgroundColor: MaterialStateProperty.all<Color>(AppTheme.secondary),
        ),
        child: Text(
          widget.title,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 18),
        ));
  }
}
