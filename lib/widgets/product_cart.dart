import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/home/product_detail_screen.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:chynar_mobile/widgets/count.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ProductCart extends StatefulWidget {
  final ProductModel product;
  final bool deletable;

  const ProductCart({Key key, this.product, this.deletable}) : super(key: key);

  @override
  _ProductCartState createState() => _ProductCartState();
}

class _ProductCartState extends State<ProductCart> {
  String fileUrl = Constants.fileServerUrl;

  @override
  Widget build(BuildContext context) {
    var product = widget.product;
    return StoreConnector<AppState, ViewModel>(
        distinct: false,
        converter: (store) => ViewModel.fromStore(store),
        builder: (BuildContext context, ViewModel vm) {
          return Stack(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 1,
                      child: Center(
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(6.0),
                            child: FadeInImage(
                              image: NetworkImage("$fileUrl${product.image}"),
                              placeholder:
                                  AssetImage("assets/images/transparent.png"),
                              fit: BoxFit.contain,
                              height: 120,
                              width: 120,
                              imageErrorBuilder: (BuildContext context,
                                  Object exception, StackTrace stackTrace) {
                                return Container(
                                  width: 50,
                                  height: 50,
                                );
                              },
                            )),
                      ),
                    ),
                    Flexible(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    vm.lang == 'en'
                                        ? product.nameEn
                                        : vm.lang == 'ru'
                                            ? product.nameRu
                                            : product.nameTm,
                                    softWrap: true,
                                    maxLines: 2,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16,
                                    )),
                                widget.deletable
                                    ? InkWell(
                                        onTap: () async {
                                          await vm.removeFromCart(product.id);
                                        },
                                        child: Icon(
                                          Icons.delete_sharp,
                                          color: HexColor('#393939'),
                                        ))
                                    : _likeWidget(vm, product)
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: widget.deletable
                                  ? [_likeWidget(vm, product)]
                                  : [],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      vm.getLangValue('price'),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      '${product.price} TMT',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700),
                                    )
                                  ],
                                ),
                                widget.deletable
                                    ? Count(
                                        product: product,
                                        hideable: false,
                                        increment: () =>
                                            _increment(vm, product),
                                        decrement: () =>
                                            _decrement(vm, product),
                                      )
                                    : Expanded(
                                        child: Padding(
                                        padding:
                                            const EdgeInsets.only(left: 20.0),
                                        child: AppButton(
                                            onClick: () {
                                              Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ProductDetailScreen(
                                                            product: product,
                                                          )));
                                            },
                                            title:
                                                vm.getLangValue('add_to_cart')),
                                      ))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        });
  }

  bool _liked(ViewModel vm, ProductModel pro) {
    bool isExist = vm.favorites.any((element) => element.id == pro.id);
    return isExist;
  }

  void _increment(ViewModel vm, ProductModel product) {
    vm.addToCart(
        product,
        widget.product.unitId == 12
            ? widget.product.amountInCart + 1
            : widget.product.amountInCart + 0.5);
  }

  void _decrement(ViewModel vm, ProductModel product) {
    vm.addToCart(
        product,
        widget.product.unitId == 12
            ? widget.product.amountInCart - 1
            : widget.product.amountInCart - 0.5);
  }

  Widget _likeWidget(ViewModel vm, ProductModel product) {
    return InkWell(
        onTap: () {
          if (_liked(vm, product)) {
            vm.removeFromFavorites(product);
          } else {
            vm.addToFavorites(product);
          }
        },
        child: Icon(
          _liked(vm, product)
              ? Icons.favorite_outlined
              : Icons.favorite_border_outlined,
          color: Colors.red,
        ));
  }
}
