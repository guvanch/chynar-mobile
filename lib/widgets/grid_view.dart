import 'dart:convert';

import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/screens/home/product_detail_screen.dart';
import 'package:chynar_mobile/widgets/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class CustomGrid extends StatefulWidget {
  final List<ProductModel> items;

  const CustomGrid({Key key, this.items}) : super(key: key);

  @override
  _CustomGridState createState() => _CustomGridState();
}

class _CustomGridState extends State<CustomGrid> {
  @override
  Widget build(BuildContext context) {
    List<ProductModel> items = widget.items;
    return StaggeredGridView.countBuilder(
        crossAxisCount: 4,
        itemCount: items.length,
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          final product = items[index];
          return new InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            ProductDetailScreen(product: product)));
              },
              child: Product(product: product));
        },
        staggeredTileBuilder: (int index) => new StaggeredTile.fit(2),
        mainAxisSpacing: 20.0,
        crossAxisSpacing: 20.0);
  }
}
