import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/screens/profile/order_detail_screen.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';

class Order extends StatefulWidget {
  final OrderModel order;
  final ViewModel vm;

  const Order({Key key, this.order, this.vm}) : super(key: key);

  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  @override
  Widget build(BuildContext context) {
    ViewModel vm = widget.vm;
    OrderModel _order = widget.order;

    if (_order.status == null) {
      _order.status = 'waiting';
    }
    if (_order.count == null) {
      _order.count = 0;
    }
    DateTime date = DateTime.parse(_order.updatedAt);
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OrderDetailScreen(order: _order)));
      },
      splashColor: AppTheme.primary.withOpacity(0.5),
      child: Container(
          margin: EdgeInsets.all(4),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 7,
                  offset: Offset(0, 2)),
            ],
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        vm.getLangValue('order_' + _order.status),
                        overflow: TextOverflow.fade,
                        style: TextStyle(
                            fontSize: 12,
                            color: _order.status == 'delivered'
                                ? AppTheme.secondary
                                : _order.status == 'canceled'
                                    ? Colors.red
                                    : HexColor('#CC7D00')),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        '${date.day}.${date.month}.${date.year}',
                        overflow: TextOverflow.fade,
                        style:
                            TextStyle(fontSize: 12, color: HexColor('#BABABA')),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      '${vm.getLangValue('order_name')} №',
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      '#' + _order.id.toString(),
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      vm.getLangValue('ordered_by'),
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      _order.user.firstname,
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      vm.getLangValue('order_count'),
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      _order.count != null ? _order.count.toString() : '',
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      vm.getLangValue('total'),
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      '${0} TMT',
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
