import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/home/product_detail_screen.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_fonts/google_fonts.dart';

class DataSearch extends SearchDelegate<String> {
  final List<ProductModel> products;
  final MarketModel market;

  DataSearch({this.products, this.market});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          padding: EdgeInsets.only(right: 10),
          icon: Icon(Icons.clear, color: AppTheme.primary),
          onPressed: () {
            query = '';
          }),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back_ios, size: 20, color: AppTheme.primary),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(height: 0, width: 0);
  }

  @override
  String get searchFieldLabel => '';

  @override
  Widget buildSuggestions(BuildContext context) {
    var suggestionList = products;

    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        suggestionList = products
            .where((p) => p.nameEn.toLowerCase().contains(query.toLowerCase()))
            .toList();
        return ListView.builder(
          itemBuilder: (context, index) => ListTile(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProductDetailScreen(
                            product: products[index],
                            market: market,
                          )));
            },
            title: RichText(
                text: TextSpan(
              text: suggestionList[index].nameEn,
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontFamily: GoogleFonts.poppins().fontFamily,
              ),
            )),
          ),
          itemCount: suggestionList.length,
        );
      },
    );
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme.copyWith(
        textTheme: TextTheme(
          headline6: TextStyle(color: Colors.black, fontSize: 18),
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: InputBorder.none,
          errorBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
        ));
  }
}
