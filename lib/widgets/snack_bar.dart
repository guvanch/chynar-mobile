import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class SnackBarContent extends StatefulWidget {
  final Function onClick;

  const SnackBarContent({Key key, @required this.onClick}) : super(key: key);

  @override
  _SnackBarContentState createState() => _SnackBarContentState();
}

class _SnackBarContentState extends State<SnackBarContent> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
        distinct: false,
        converter: (store) => ViewModel.fromStore(store),
        builder: (BuildContext context, ViewModel vm) {
          return Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(vm.getLangValue('added_to_cart')),
                TextButton(
                    onPressed: widget.onClick,
                    child: Text(
                      vm.getLangValue('view_cart'),
                      style: TextStyle(color: Colors.blue),
                    ))
              ],
            ),
          );
        });
  }
}
