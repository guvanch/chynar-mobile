import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:flutter/material.dart';

class RadioButton extends StatefulWidget {
  final String title;
  final bool checked;
  final Function onClick;
  final double border;

  const RadioButton(
      {Key key,
      @required this.title,
      @required this.checked,
      @required this.onClick,
      @required this.border})
      : super(key: key);

  @override
  _RadioButtonState createState() => _RadioButtonState();
}

class _RadioButtonState extends State<RadioButton> {
  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
        style: TextButton.styleFrom(
          padding: EdgeInsets.all(12),
          minimumSize: Size(double.infinity, 36),
          primary: HexColor('#FF9105'),
          side: BorderSide(color: HexColor('#D8D8D8'), width: 1.5),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(widget.border),
              side: BorderSide(color: Colors.red)),
        ),
        onPressed: widget.onClick,
        icon: Icon(
          Icons.add,
          size: 0,
        ),
        label: Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.title,
                style: TextStyle(color: Colors.black),
              ),
              Icon(
                widget.checked
                    ? Icons.radio_button_checked_sharp
                    : Icons.radio_button_off_outlined,
                color:
                    widget.checked ? HexColor('#FF9105') : HexColor('#D8D8D8'),
              )
            ],
          ),
        ));
  }
}
