import 'package:chynar_mobile/models/user_profile_model.dart';

class UserAddressModel {
  int id;
  String recName;
  String recAddress;
  String recNumber;
  String createdAt;
  String updatedAt;
  int userId;
  UserProfileModel userProfile;

  UserAddressModel(
      {this.id,
      this.recName,
      this.recAddress,
      this.recNumber,
      this.createdAt,
      this.updatedAt,
      this.userId,
      this.userProfile});

  UserAddressModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    recName = json['rec_name'];
    recAddress = json['rec_address'];
    recNumber = json['rec_number'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    userId = json['UserId'];
    if (json['User'] != null) {
      userProfile = UserProfileModel.fromJson(json['User']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['rec_name'] = this.recName;
    data['rec_address'] = this.recAddress;
    data['rec_number'] = this.recNumber;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['UserId'] = this.userId;
    data['User'] = this.userProfile;
    return data;
  }
}
