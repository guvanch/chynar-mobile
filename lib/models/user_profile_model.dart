import 'package:chynar_mobile/models/user_permission_model.dart';
import 'package:chynar_mobile/models/user_type_model.dart';

class UserProfileModel {
  int id;
  String firstname;
  String lastname;
  String phoneNumber;
  int primaryAddressId;
  int userTypeId;

  UserProfileModel(
      {this.id,
      this.firstname,
      this.lastname,
      this.phoneNumber,
      this.primaryAddressId,
      this.userTypeId});

  UserProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstname = json['fname'];
    lastname = json['lastname'];
    phoneNumber = json['phoneNumber'];
    primaryAddressId = json['primary_addres_id'];
    userTypeId = json['UserTypeId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['fname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['phoneNumber'] = this.phoneNumber;
    data['primary_addres_id'] = this.primaryAddressId;
    data['UserTypeId'] = this.userTypeId;
    return data;
  }
}
