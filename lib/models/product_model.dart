import 'package:chynar_mobile/models/config_model.dart';
import 'package:chynar_mobile/models/market_category_model.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/unit_model.dart';

class ProductModel {
  int id;
  String nameTm;
  String nameRu;
  String nameEn;
  int price;
  int salePrice;
  String step;
  String articleTm;
  String articleRu;
  String articleEn;
  String descriptionTm;
  String descriptionRu;
  String descriptionEn;
  bool isSale;
  String saleUntil;
  bool isActive;
  int totalAmount;
  int viewCount;
  bool isValutaPrice;
  String search;
  String image;
  bool isNew;
  String createdAt;
  String updatedAt;
  int marketId;
  int marketCategoryId;
  int unitId;
  int configId;
  MarketModel market;
  MarketCategoryModel marketCategory;
  UnitModel unit;
  ConfigModel config;
  double amountInCart;

  ProductModel(
      {this.id,
      this.nameTm,
      this.nameRu,
      this.nameEn,
      this.price,
      this.salePrice,
      this.step,
      this.articleTm,
      this.articleRu,
      this.articleEn,
      this.descriptionTm,
      this.descriptionRu,
      this.descriptionEn,
      this.isSale,
      this.saleUntil,
      this.isActive,
      this.totalAmount,
      this.viewCount,
      this.isValutaPrice,
      this.search,
      this.image,
      this.isNew,
      this.createdAt,
      this.updatedAt,
      this.marketId,
      this.marketCategoryId,
      this.unitId,
      this.configId,
      this.market,
      this.marketCategory,
      this.unit,
      this.config,
      this.amountInCart});

  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameTm = json['name_tm'];
    nameRu = json['name_ru'];
    nameEn = json['name_en'];
    price = json['price'];
    salePrice = json['sale_price'];
    step = json['step'];
    articleTm = json['article_tm'];
    articleRu = json['article_ru'];
    articleEn = json['article_en'];
    descriptionTm = json['description_tm'];
    descriptionRu = json['description_ru'];
    descriptionEn = json['description_en'];
    isSale = json['is_sale'];
    saleUntil = json['sale_until'];
    isActive = json['is_active'];
    totalAmount = json['total_amount'];
    viewCount = json['view_count'];
    isValutaPrice = json['is_valyuta_price'];
    search = json['search'];
    image = json['surat'];
    isNew = json['is_new'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    marketId = json['MarketId'];
    marketCategoryId = json['MarketKategoriyaId'];
    unitId = json['UnitId'];
    configId = json['ConfigId'];

    if (json['Market'] != null) {
      market = MarketModel.fromJson(json['Market']);
    }
    if (json['MarketKategoriya'] != null) {
      marketCategory = MarketCategoryModel.fromJson(json['MarketKategoriya']);
    }
    if (json['Unit'] != null) {
      unit = UnitModel.fromJson(json['Unit']);
    }
    if (json['Config'] != null) {
      config = ConfigModel.fromJson(json['Config']);
    }

    amountInCart = 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_tm'] = this.nameTm;
    data['name_ru'] = this.nameRu;
    data['name_en'] = this.nameEn;
    data['price'] = this.price;
    data['sale_price'] = this.salePrice;
    data['step'] = this.step;
    data['article_tm'] = this.articleTm;
    data['article_ru'] = this.articleRu;
    data['article_en'] = this.articleEn;
    data['description_tm'] = this.descriptionTm;
    data['description_ru'] = this.descriptionRu;
    data['description_en'] = this.descriptionEn;
    data['is_sale'] = this.isSale;
    data['sale_until'] = this.saleUntil;
    data['is_active'] = this.isActive;
    data['total_amount'] = this.totalAmount;
    data['view_count'] = this.viewCount;
    data['is_valyuta_price'] = this.isValutaPrice;
    data['search'] = this.search;
    data['surat'] = this.image;
    data['is_new'] = this.isNew;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['MarketId'] = this.marketId;
    data['MarketKategoriyaId'] = this.marketCategoryId;
    data['UnitId'] = this.unitId;
    data['Market'] = this.market;
    data['MarketKategoriya'] = this.marketCategory;
    data['Unit'] = this.unit;
    data['Config'] = this.config;

    return data;
  }
}
