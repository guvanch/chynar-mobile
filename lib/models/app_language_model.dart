import 'package:shared_preferences/shared_preferences.dart';

class AppLanguageModel {
  final Map<String, List<Map<String, String>>> _languages = {
    'ru': [
      {'code': 'en', 'name': 'Английский'},
      {'code': 'ru', 'name': 'Русский'},
      {'code': 'tm', 'name': 'туркменский'},
    ],
    'en': [
      {'code': 'th', 'name': 'Thai'},
      {'code': 'en', 'name': 'English'},
      {'code': 'tm', 'name': 'Turkmen'},
    ],
    'tm': [
      {'code': 'en', 'name': 'Iňlis dili'},
      {'code': 'ru', 'name': 'Rus dili'},
      {'code': 'tm', 'name': 'Türkmen'},
    ]
  };
  String _selectedLanguage = '';

  final Map<String, Map<String, String>> _languagesValues = {
    'en': {
      'invalid_email': 'Valid email is required',
      'password_too_short': 'Password is too short',
      'firstname_too_short': 'First Name is too short',
      'lastname_too_short': 'Last Name is too short',
      'service_too_busy': 'Service is busy, please - try later...',
      'user_not_found': 'User not found',
      'not_confirmed_email': 'This Email is not confirmed',
      'bad_password': 'Incorrect password',
      'verified_already': 'User is verified already',
      'email_isnt_verified_yet': 'User is not verified yet',
      'invalid_token': 'Invalid Token',
      'wrong_token': 'Wrong Token',
      'token_already_used': 'Token already used',
      'forbidden': 'Forbidden',
      'user_already_exists': 'User exists already',
      'cancel': 'Cancel',
      'continue': 'Continue',
      'confirm': 'Confirm',
      'error': 'Error',
      'update': 'Update',
      'auth_error': 'Authentication error',
      'auth_email_error_empty': 'Email is required',
      'auth_email_error_invalid': 'Email is incorrect',
      'auth_password_error_empty': 'Password is required',
      'auth_logout_confirm': 'Are you sure you want to logout?',
      'auth_email_confirmation_sent': 'Email confirmation link was sent',
      'http_common_error': 'Error is occurred during requesting data...',
      'bottom_nav_title_home': 'Home',
      'bottom_nav_title_history': 'History',
      'bottom_nav_title_profile': 'Profile',
      'bottom_nav_title_language': 'Language',
      'bottom_nav_title_signin': 'Sign In',
      'choice_nav_title_topup': 'TOP UP',
      'choice_nav_title_gift_cards': 'GIFT CARDS',
      'login_page_primary_button_register': 'Sign Up',
      'login_page_primary_button_login': 'Sign In',
      'login_page_primary_button_forgot': 'Reset password',
      'login_page_bottom_button_register': 'Create an Account',
      'login_page_bottom_button_forgot': 'Forgot password?',
      'login_page_bottom_button_login': 'Have an account?',
      'login_page_field_email': 'Email',
      'login_page_field_password': 'Password',
      'profile_nav_title_personal': 'Personal data',
      'profile_nav_title_settings': 'Settings',
      'profile_nav_title_phones': 'My Phones',
      'profile_nav_title_email': 'Confirm Email',
      'profile_nav_title_logout': 'Log out',
      'person_page_field_email': 'Email',
      'person_page_field_first_name': 'First Name',
      'person_page_field_last_name': 'Last Name',
      'person_page_field_born_date': 'Born date',
      'person_page_field_first_name_error':
          'First Name may be from 2 to 32 characters length',
      'person_page_field_last_name_error':
          'Last Name may be from 2 to 32 characters length',
      'confirm_email_field_token': 'Token from Email',
      'confirm_email_error_empty': 'Token from Email is required',
      'confirm_email_success': 'Email is confirmed',
      'confirm_email_request': 'Please, confirm Email',
      'drop_password_email_sent':
          'A password reset link has been sent to <email>',
    },
    'ru': {
      'invalid_email': 'Требуется корректный Email',
      'password_too_short': 'Очень короткий пароль',
      'firstname_too_short': 'Короткое имя',
      'lastname_too_short': 'Короткая фамилия',
      'service_too_busy':
          'Наш сервис сейчас занят - пожалуйста, попробуйте позже...',
      'user_not_found': 'Пользователь не найден',
      'not_confirmed_email': 'Email не подтвержден',
      'bad_password': 'Неправильный пароль',
      'verified_already': 'Пользователь уже прошел проверку',
      'email_isnt_verified_yet': 'Пользователь еще не прошел проверку',
      'invalid_token': 'Неверный ключ',
      'wrong_token': 'Неверный код',
      'token_already_used': 'Ключ уже был использован',
      'forbidden': 'Запрещено',
      'user_already_exists': 'Пользователь уже существует',
      'cancel': 'Отменить',
      'continue': 'Продолжить',
      'confirm': 'Подтвердить',
      'error': 'Ошибка',
      'update': 'Обновить',
      'auth_error': 'Ошибка аутентификации',
      'auth_email_error_empty': 'Требуется Email',
      'auth_email_error_invalid': 'Email некорректен',
      'auth_password_error_empty': 'Требуется пароль',
      'auth_logout_confirm': 'Вы хотите выйти?',
      'auth_email_confirmation_sent': 'Отправлена ссылка подтверждения Email',
      'http_common_error': 'Во время выполнения запроса произошла ошибка...',
      'bottom_nav_title_home': 'Главная',
      'bottom_nav_title_history': 'История',
      'bottom_nav_title_profile': 'Профиль',
      'bottom_nav_title_language': 'Язык',
      'bottom_nav_title_signin': 'Войти',
      'choice_nav_title_topup': 'Пополнить номер',
      'choice_nav_title_gift_cards': 'Подарочные карты',
      'login_page_primary_button_register': 'Зарегистрироваться',
      'login_page_primary_button_login': 'Войти',
      'login_page_primary_button_forgot': 'Сбросить пароль',
      'login_page_bottom_button_register': 'Регистрация',
      'login_page_bottom_button_forgot': 'Забыли пароль?',
      'login_page_bottom_button_login': 'Уже есть учетка?',
      'login_page_field_email': 'Email',
      'login_page_field_password': 'Пароль',
      'profile_nav_title_personal': 'Персональные данные',
      'profile_nav_title_settings': 'Настройки',
      'profile_nav_title_phones': 'Мои номера',
      'profile_nav_title_email': 'Подтвердить Email',
      'profile_nav_title_logout': 'Выйти',
      'person_page_field_email': 'Email',
      'person_page_field_first_name': 'Имя',
      'person_page_field_last_name': 'Фамилия',
      'person_page_field_born_date': 'Дата рождения',
      'person_page_field_first_name_error':
          'Имя может иметь длину от 2 до 32 символов',
      'person_page_field_last_name_error':
          'Фамилия может иметь длину от2 до 32 символов',
      'confirm_email_field_token': 'Код из Email',
      'confirm_email_error_empty': 'Нужно ввести код из Email',
      'confirm_email_success': 'Email подтвержден',
      'confirm_email_request': 'Подтвердите Email пожалуйста',
      'drop_password_email_sent':
          'Ссылка для сброса пароля отправлена на <email>',
    },
    'tm': {
      'invalid_email': 'Valid email is required',
      'password_too_short': 'Password is too short',
      'firstname_too_short': 'First Name is too short',
      'lastname_too_short': 'Last Name is too short',
      'service_too_busy': 'Service is busy, please - try later...',
      'user_not_found': 'User not found',
      'not_confirmed_email': 'This Email is not confirmed',
      'bad_password': 'Incorrect password',
      'verified_already': 'User is verified already',
      'email_isnt_verified_yet': 'User is not verified yet',
      'invalid_token': 'Invalid Token',
      'wrong_token': 'Wrong Token',
      'token_already_used': 'Token already used',
      'forbidden': 'Forbidden',
      'user_already_exists': 'User exists already',
      'cancel': 'Cancel',
      'continue': 'Continue',
      'confirm': 'Confirm',
      'error': 'Error',
      'update': 'Update',
      'auth_error': 'Authentication error',
      'auth_email_error_empty': 'Email is required',
      'auth_email_error_invalid': 'Email is incorrect',
      'auth_password_error_empty': 'Password is required',
      'auth_logout_confirm': 'Are you sure you want to logout?',
      'auth_email_confirmation_sent': 'Email confirmation link was sent',
      'http_common_error': 'Error is occurred during requesting data...',
      'bottom_nav_title_home': 'Home',
      'bottom_nav_title_history': 'History',
      'bottom_nav_title_profile': 'Profile',
      'bottom_nav_title_language': 'Language',
      'bottom_nav_title_signin': 'Sign In',
      'choice_nav_title_topup': 'TOP UP',
      'choice_nav_title_gift_cards': 'GIFT CARDS',
      'login_page_primary_button_register': 'Sign Up',
      'login_page_primary_button_login': 'Sign In',
      'login_page_primary_button_forgot': 'Reset password',
      'login_page_bottom_button_register': 'Create an Account',
      'login_page_bottom_button_forgot': 'Forgot password?',
      'login_page_bottom_button_login': 'Have an account?',
      'login_page_field_email': 'Email',
      'login_page_field_password': 'Password',
      'profile_nav_title_personal': 'Personal data',
      'profile_nav_title_settings': 'Settings',
      'profile_nav_title_phones': 'My Phones',
      'profile_nav_title_email': 'Confirm Email',
      'profile_nav_title_logout': 'Log out',
      'person_page_field_email': 'Email',
      'person_page_field_first_name': 'First Name',
      'person_page_field_last_name': 'Last Name',
      'person_page_field_born_date': 'Born date',
      'person_page_field_first_name_error':
          'First Name may be from 2 to 32 characters length',
      'person_page_field_last_name_error':
          'Last Name may be from 2 to 32 characters length',
      'confirm_email_field_token': 'Token from Email',
      'confirm_email_error_empty': 'Token from Email is required',
      'confirm_email_success': 'Email is confirmed',
      'confirm_email_request': 'Please, confirm Email',
      'drop_password_email_sent':
          'A password reset link has been sent to <email>',
    },
  };

  SharedPreferences _sharedPrefs;

  List<Map<String, String>> get languagesList => _languages[_selectedLanguage];
  String get selectedLanguage => _selectedLanguage;
  String get selectedLanguageName {
    var list = _languages[_selectedLanguage];
    for (var item in list) {
      if (item['code'] == _selectedLanguage) {
        return item['name'];
      }
    }
    return _selectedLanguage;
  }

  Future<void> selectLanguage(String languageKey) async {
    if (_selectedLanguage != languageKey && _languages[languageKey] != null) {
      String _old = _selectedLanguage;
      _selectedLanguage = languageKey;
      try {
        await _sharedPrefs.setString('lang', _selectedLanguage);
      } catch (e) {
        _selectedLanguage = _old;
      }
    }
  }

  String langValue(String value) {
    if (_languagesValues[_selectedLanguage] == null) return value;
    return _languagesValues[_selectedLanguage][value] ?? value;
  }
}
