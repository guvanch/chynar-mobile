class MarketAddress {
  int id;
  String nameTm;
  String nameRu;
  String descriptionTm;
  String descriptionRu;
  String descriptionEn;
  String createdAt;
  String updatedAt;
  int marketId;

  MarketAddress({
    this.id,
    this.nameTm,
    this.nameRu,
    this.descriptionTm,
    this.descriptionRu,
    this.descriptionEn,
    this.createdAt,
    this.updatedAt,
    this.marketId,
  });

  MarketAddress.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameTm = json['name_tm'];
    nameRu = json['name_ru'];
    descriptionTm = json['description_tm'];
    descriptionRu = json['description_ru'];
    descriptionEn = json['description_en'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    marketId = json['MarketId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_tm'] = this.nameTm;
    data['name_ru'] = this.nameRu;
    data['description_tm'] = this.descriptionTm;
    data['description_ru'] = this.descriptionRu;
    data['description_en'] = this.descriptionEn;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['MarketId'] = this.marketId;
    return data;
  }
}
