import 'package:chynar_mobile/models/ordered__product_modell.dart';

class NewOrderModel {
  int sum;
  bool isCash;
  String orderDateTime;
  String deliveryDateTime;
  int userId;
  int addressId;
  bool delivered;
  List<OrderedProductModel> orderedProducts;

  NewOrderModel(
      {this.sum,
      this.isCash,
      this.orderDateTime,
      this.deliveryDateTime,
      this.userId,
      this.addressId,
      this.delivered,
      this.orderedProducts});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sum'] = this.sum;
    data['is_cash'] = this.isCash;
    data['order_date_time'] = this.orderDateTime;
    data['delivery_date_time'] = this.deliveryDateTime;
    data['UserId'] = this.userId;
    data['AddressId'] = this.userId;
    data['delivered'] = this.delivered;
    data['orderedProducts'] = this.orderedProducts;
    return data;
  }
}
