class ConfigModel {
  int id;
  String currencyExchange;

  ConfigModel({
    this.id,
    this.currencyExchange,
  });

  ConfigModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    currencyExchange = json['currency_exchange'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['currency_exchange'] = this.currencyExchange;
    return data;
  }
}
