class OrderedProductModel {
  double amount;
  int productId;
  bool delivered;

  OrderedProductModel({this.amount, this.productId, this.delivered});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['delivered'] = this.delivered;
    data['ProductId'] = this.productId;

    return data;
  }
}
