import 'package:chynar_mobile/models/phone_model.dart';

import 'market_address_model.dart';

class MarketModel {
  int id;
  String nameTm;
  String nameRu;
  String nameEn;
  String surat;
  String createdAt;
  String updatedAt;
  List<PhoneNumber> phoneNumbers;
  List<MarketAddress> marketAddresses;

  MarketModel(
      {this.id,
      this.nameTm,
      this.nameRu,
      this.nameEn,
      this.surat,
      this.createdAt,
      this.updatedAt,
      this.phoneNumbers,
      this.marketAddresses});

  MarketModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameTm = json['name_tm'];
    nameRu = json['name_ru'];
    nameEn = json['name_en'];
    surat = json['surat'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    if (json['PhoneNumbers'] != null) {
      phoneNumbers = [];
      json['PhoneNumbers'].forEach((v) {
        phoneNumbers.add(new PhoneNumber.fromJson(v));
      });
    }
    if (json['MarketAddresses'] != null) {
      marketAddresses = [];
      json['MarketAddresses'].forEach((v) {
        marketAddresses.add(new MarketAddress.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_tm'] = this.nameTm;
    data['name_ru'] = this.nameRu;
    data['name_en'] = this.nameEn;
    data['surat'] = this.surat;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['PhoneNumbers'] = this.phoneNumbers;
    data['MarketAddresses'] = this.marketAddresses;
    return data;
  }
}
