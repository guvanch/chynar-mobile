class UserPermissionModel {
  int id;
  int number;

  UserPermissionModel({
    this.id,
    this.number,
  });

  UserPermissionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    number = json['number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['number'] = this.number;
    return data;
  }
}
