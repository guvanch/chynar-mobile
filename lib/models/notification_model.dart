class NotificationModel {
  int id;
  String action;

  NotificationModel({this.id, this.action});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    action = json['action'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['id'] = this.id;
    data['action'] = this.action;

    return data;
  }
}
