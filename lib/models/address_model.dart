class AddressModel {
  int id;
  String address;
  String name;
  String phoneNumber;
  bool isMain;

  AddressModel(
      {this.id, this.address, this.name, this.phoneNumber, this.isMain});

  AddressModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    address = json['address'];
    name = json['name'];
    phoneNumber = json['phoneNumber'];
    isMain = json['isMain'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['address'] = this.address;
    data['name'] = this.name;
    data['phoneNumber'] = this.phoneNumber;
    data['isMain'] = this.isMain;
    return data;
  }
}
