class NewUserModel {
  int id;
  String token;
  String phoneNumber;
  int typeId;
  String msg;

  NewUserModel({this.id, this.token, this.typeId, this.phoneNumber, this.msg});

  NewUserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    token = json['token'];
    phoneNumber = json['phoneNumber'];
    typeId = json['UserTypeId'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['token'] = this.token;
    data['phoneNumber'] = this.phoneNumber;
    data['UserTypeId'] = this.typeId;
    data['msg'] = this.msg;
    return data;
  }
}
