import 'package:chynar_mobile/models/user_permission_model.dart';
import 'package:chynar_mobile/models/user_type_model.dart';

class UserModel {
  int id;
  String name;
  String token;
  UserTypeModel type;
  int typeId;
  List<UserPermissionModel> permission;
  bool login;
  String msg;

  UserModel(
      {this.id,
      this.name,
      this.token,
      this.type,
      this.typeId,
      this.permission,
      this.login,
      this.msg});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    token = json['token'];
    if (json['type'] != null) {
      type = UserTypeModel.fromJson(json['type']);
    }
    typeId = json['typeID'];
    if (json['permission'] != null) {
      permission = [];
      json['permission'].forEach((v) {
        permission.add(new UserPermissionModel.fromJson(v));
      });
    }
    login = json['login'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['token'] = this.token;
    data['type'] = this.type;
    data['login'] = this.login;
    data['msg'] = this.msg;
    return data;
  }
}
