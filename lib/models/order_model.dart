import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/user_profile_model.dart';

class OrderModel {
  int id;
  int sum;
  int count;
  bool isCash;
  String orderDateTime;
  String deliveryDateTime;
  bool delivered;
  String createdAt;
  String updatedAt;
  int userId;
  int addressId;
  int statusId;
  List<ProductModel> orderedProducts;
  String status;
  UserProfileModel user;

  OrderModel({
    this.id,
    this.sum,
    this.count,
    this.isCash,
    this.orderDateTime,
    this.deliveryDateTime,
    this.delivered,
    this.createdAt,
    this.updatedAt,
    this.userId,
    this.addressId,
    this.statusId,
    this.orderedProducts,
    this.status,
    this.user,
  });

  OrderModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sum = json['sum'];
    count = json['sany'];
    isCash = json['is_cash'];
    orderDateTime = json['order_date_time'];
    deliveryDateTime = json['delivery_date_time'];
    delivered = json['delivered'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    userId = json['UserId'];
    addressId = json['AddressId'];
    statusId = json['StatusId'];

    if (json['OrderedProducts'] != null && json['OrderedProducts'].length > 0) {
      orderedProducts = [];
      json['OrderedProducts'].forEach((v) {
        orderedProducts.add(new ProductModel.fromJson(v));
      });
    }

    status = json['Status'];
    user = UserProfileModel.fromJson(json['User']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['id'] = this.id;
    data['sum'] = this.sum;
    data['sany'] = this.count;
    data['is_cash'] = this.isCash;
    data['order_date_time'] = this.orderDateTime;
    data['delivery_date_time'] = this.deliveryDateTime;
    data['delivered'] = this.delivered;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['UserId'] = this.userId;
    data['AddressId'] = this.addressId;
    data['StatusId'] = this.statusId;
    data['OrderedProducts'] = this.orderedProducts;
    data['Status'] = this.status;
    data['User'] = this.user;
    return data;
  }
}
