import 'package:chynar_mobile/models/market_model.dart';

class MarketCategoryModel {
  int id;
  String nameTm;
  String nameRu;
  String nameEn;
  bool active;
  String createdAt;
  String updatedAt;
  int marketId;
  MarketModel market;

  MarketCategoryModel(
      {this.id,
      this.nameTm,
      this.nameRu,
      this.nameEn,
      this.active,
      this.createdAt,
      this.updatedAt,
      this.marketId,
      this.market});

  MarketCategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameTm = json['name_tm'];
    nameRu = json['name_ru'];
    nameEn = json['name_en'];
    active = json['active'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    marketId = json['MarketId'];
    if (json['Market'] != null) {
      market = MarketModel.fromJson(json['Market']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_tm'] = this.nameTm;
    data['name_ru'] = this.nameRu;
    data['name_en'] = this.nameEn;
    data['active'] = this.active;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['MarketId'] = this.marketId;
    data['Market'] = this.market;
    return data;
  }
}
