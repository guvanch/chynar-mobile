import 'package:chynar_mobile/models/user_permission_model.dart';

class UserTypeModel {
  String typeTm;
  String typeRu;
  String typeEn;
  List<UserPermissionModel> permissions;

  UserTypeModel({
    this.typeTm,
    this.typeEn,
    this.typeRu,
    this.permissions,
  });

  UserTypeModel.fromJson(Map<String, dynamic> json) {
    typeTm = json['type_tm'];
    typeEn = json['type_en'];
    typeRu = json['type_ru'];
    if (json['Permissions'] != null) {
      permissions = [];
      json['Permissions'].forEach((v) {
        permissions.add(new UserPermissionModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type_tm'] = this.typeTm;
    data['type_en'] = this.typeEn;
    data['type_ru'] = this.typeRu;
    data['Permissions'] = this.permissions;
    return data;
  }
}
