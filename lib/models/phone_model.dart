class PhoneNumber {
  int id;
  String phoneNumber;

  PhoneNumber({
    this.id,
    this.phoneNumber,
  });

  PhoneNumber.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    phoneNumber = json['phoneNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['phoneNumber'] = this.phoneNumber;
    return data;
  }
}
