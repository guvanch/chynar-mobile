import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/cart/cart_screen.dart';
import 'package:chynar_mobile/screens/home/home_screen.dart';
import 'package:chynar_mobile/screens/liked/liked_screen.dart';
import 'package:chynar_mobile/screens/profile/profile_screen.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/custom_navbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppContainer extends StatefulWidget {
  @override
  _AppContainerState createState() => _AppContainerState();
}

class _AppContainerState extends State<AppContainer> {
  String activeTab = 'home';

  int activePage = 0;
  final PageController pageController =
      PageController(initialPage: 0, keepPage: true);

  PersistentTabController _controller =
      PersistentTabController(initialIndex: 0);

  @override
  initState() {
    super.initState();
  }

  changeTab(tab) {
    setState(() {
      activeTab = tab;
    });
  }

  Widget _render(ViewModel vm) {
    return PersistentTabView.custom(context,
        controller: _controller,
        itemCount:
            4, // This is required in case of custom style! Pass the number of items for the nav bar.
        screens: _buildScreens(),
        confineInSafeArea: true,
        handleAndroidBackButtonPress: true,
        customWidget: CustomNavBarWidget(
          // Your custom widget goes here
          items: _navBarsItems(vm),
          selectedIndex: _controller.index,
          onItemSelected: (index) {
            setState(() {
              _controller.index =
                  index; // NOTE: THIS IS CRITICAL!! Don't miss it!
            });
          },
        ));
  }

  List<PersistentBottomNavBarItem> _navBarsItems(ViewModel vm) {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(
          Icons.home,
          size: 20,
        ),
        title: (vm.getLangValue('home_page')),
        activeColorPrimary: AppTheme.primary,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(
          FontAwesomeIcons.shoppingCart,
          size: 20,
        ),
        title: (vm.getLangValue('cart_page')),
        activeColorPrimary: AppTheme.primary,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(
          Icons.favorite_outline_outlined,
          size: 20,
        ),
        title: (vm.getLangValue('liked_page')),
        activeColorPrimary: AppTheme.primary,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(
          Icons.account_circle_outlined,
          size: 20,
        ),
        title: (vm.getLangValue('profile_page')),
        activeColorPrimary: AppTheme.primary,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  List<Widget> _buildScreens() {
    return [HomeScreen(), CartScreen(), LikedScreen(), ProfileScreen()];
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        // Future.delayed(new Duration(milliseconds: 1000), () async {
        try {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          String lang = prefs.getString('lang');
          if (lang != null) {
            vm.setLang(lang);
          } else {
            vm.setLang('tm');
          }
        } catch (e) {
          print(e.toString());
          vm.setLang('tm');
        }
        // });
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(vm);
      },
    );
  }
}
