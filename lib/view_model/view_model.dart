import 'dart:math';

import 'package:chynar_mobile/models/address_model.dart';
import 'package:chynar_mobile/models/market_category_model.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/notification_model.dart';
import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/models/user_model.dart';
import 'package:chynar_mobile/redux/actions/lang_actions.dart';
import 'package:chynar_mobile/redux/actions/market_actions.dart';
import 'package:chynar_mobile/redux/actions/market_categories_action.dart';
import 'package:chynar_mobile/redux/actions/products_action.dart';
import 'package:chynar_mobile/redux/actions/user_actions.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/services/market_service.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';

class ViewModel {
  final UserModel user;
  final String token;
  final bool isLogin;
  final String lang;
  final List<UserAddressModel> userAddresses;
  final List<NotificationModel> nots;
  final List<MarketModel> markets;
  final List<MarketCategoryModel> marketCategories;
  final List<ProductModel> products;
  final List<ProductModel> favorites;
  final List<ProductModel> cartProducts;
  final List<AddressModel> addresses;
  final List<OrderModel> orders;
  final Function setUser;
  final Function setNots;
  final Function fetchUserAddresses;
  final Function setUserAddresses;
  final Function fetchAddresses;
  final Function setLang;
  final Function addToCart;
  final Function removeFromCart;
  final Function emptyCart;
  final Function getLangValue;
  final Function fetchMarkets;
  final Function fetchFavorites;
  final Function fetchMarketCategories;
  final Function setMarketCategories;
  final Function fetchProducts;
  final Function setProducts;
  final Function setOrders;
  final Function addToFavorites;
  final Function removeFromFavorites;

  ViewModel(
      {@required this.user,
      @required this.token,
      @required this.isLogin,
      @required this.nots,
      @required this.setNots,
      @required this.userAddresses,
      @required this.lang,
      @required this.markets,
      @required this.marketCategories,
      @required this.fetchMarkets,
      @required this.products,
      @required this.addToCart,
      @required this.removeFromCart,
      @required this.emptyCart,
      @required this.cartProducts,
      @required this.favorites,
      @required this.addresses,
      @required this.orders,
      @required this.setUser,
      @required this.fetchUserAddresses,
      @required this.setUserAddresses,
      @required this.fetchMarketCategories,
      @required this.setMarketCategories,
      @required this.fetchProducts,
      @required this.fetchFavorites,
      @required this.setProducts,
      @required this.fetchAddresses,
      @required this.setLang,
      @required this.getLangValue,
      @required this.setOrders,
      @required this.addToFavorites,
      @required this.removeFromFavorites});

  static ViewModel fromStore(Store<AppState> store) {
    final Map<String, Map<String, String>> _languagesValues = {
      'en': {
        'home_page': 'Home',
        'cart_page': 'Cart',
        'liked_page': 'Liked',
        'profile_page': 'Profile',
        'loading': 'Loading',
        'delivery': 'Delivery',
        'total': 'Total',
        'order': 'Order',
        'login': 'Login',
        'register': 'Register',
        'no_register': 'Continue without register',
        'contact_info': 'Contact Information',
        'name': 'Name',
        'firstname': 'First name',
        'lastname': 'Last name',
        'address': 'Address',
        'phone': 'Phone Number',
        'continue': 'Continue',
        'order_success': 'Your order has been accepted!',
        'delivery_will_contact': 'Our courier will contact you soon',
        'ok': 'Ok',
        'delivery_address': 'Delivery Address',
        'change': 'Change',
        'payment_type': 'Payment type',
        'pay_cash': 'Cash Payment',
        'card_payment': 'Card Payment',
        'price': 'Price',
        'discount_food': 'Great discount on fruits and vegetables',
        'delivery_only_dashoguz':
            'Delivery service is available only in Dashoguz province',
        'search': 'Search',
        'our_add_here': 'Your ad is here',
        'new': 'NEW',
        'day': 'day',
        'hour': 'hour',
        'minute': 'min',
        'kg': 'kg',
        'add_to_cart': 'Add to cart',
        'related_products': 'Related products',
        'add_new_address': 'Add new address',
        'input_info': 'Enter your information',
        'save': 'Save',
        'my_addresses': 'My addresses',
        'contact_with_us': 'Contact with us',
        'enter_comment': 'Enter you comment',
        'send': 'Send',
        'change_address': 'Change address',
        'enter_info': 'Enter you information',
        'change_lang': 'Change language',
        'salam': 'Welcome',
        'my_orders': 'My orders',
        'logout': 'Logout',
        'main_address': 'Main address',
        'not_found': 'Not found',
        'order_name': 'Order',
        'ordered_by': 'Ordered by',
        'order_delivered': 'Delivered',
        'order_waiting': 'Waiting',
        'order_canceled': 'Canceled',
        'order_count': 'Number of orders',
        'order_address': 'Address of order',
        'count_unit': '',
        'enter_name': 'Please enter your name',
        'enter_firstname': 'Please enter your first name',
        'enter_lastname': 'Please enter your last name',
        'enter_address': 'Please enter your address',
        'enter_phone': 'Please enter yor phone number',
        'enter_password': 'Please enter your password',
        'enter_message': 'Please enter your message',
        'add_login_details': 'Add your login details',
        'have_an_account': 'Have an Account?',
        'start_order': 'Start order',
        'new_product': 'New Product addedd to Cart',
        'new_order': 'You made a new order',
        'success': 'Success',
        'failure': 'Failed',
        'notifications': 'Notifications',
        'added_to_cart': 'Added to cart',
        'view_cart': 'View cart',
        'copied': 'Copied'
      },
      'ru': {
        'home_page': 'Главная',
        'cart_page': 'Корзина',
        'liked_page': 'Понравилось',
        'profile_page': 'профиль',
        'loading': 'Загрузка',
        'delivery': 'Доставка',
        'total': 'Всего',
        'order': 'Заказ',
        'login': 'Войти',
        'register': 'Pегистрации',
        'no_register': 'Продолжить без регистрации',
        'contact_info': 'Контактная информация',
        'name': 'Имя',
        'firstname': 'Имя',
        'lastname': ' Фамилия',
        'address': 'Адрес',
        'phone': 'Телефонный номер',
        'continue': 'Продолжать',
        'order_success': 'Ваш заказ принят!',
        'delivery_will_contact': 'Наш курьер свяжется с вами в ближайшее время',
        'ok': 'Хорошо',
        'delivery_address': 'Адрес доставки',
        'change': 'Изменять',
        'payment_type': 'Способ оплаты',
        'pay_cash': 'Наличный расчет',
        'card_payment': 'Оплата картой',
        'price': 'Цена',
        'discount_food': 'Большие скидки на фрукты и овощи',
        'delivery_only_dashoguz':
            'Услуга доставки доступна только в Дашогузском велаяте.',
        'search': 'Поиск',
        'our_add_here': 'Ваша реклама здесь',
        'new': 'новый',
        'day': 'день',
        'hour': 'час',
        'minute': 'мин',
        'kg': 'кг',
        'add_to_cart': 'Добавить в корзину',
        'related_products': 'Сопутствующие товары',
        'add_new_address': 'Добавить новый адрес',
        'input_info': 'Введите вашу информацию',
        'save': 'Сохранить',
        'my_addresses': 'Мои адреса',
        'contact_with_us': 'Свяжитесь с нами',
        'enter_comment': 'Введите свой комментарий',
        'send': 'Отправить',
        'change_address': 'Сменить адрес',
        'enter_info': 'Введите вашу информацию',
        'change_lang': 'изменение языка',
        'salam': 'Добро пожаловать',
        'my_orders': 'Мои заказы',
        'logout': 'Выйти',
        'main_address': 'основной адрес',
        'not_found': 'не найден',
        'order_name': 'Заказ',
        'ordered_by': 'Заказан',
        'order_delivered': 'Доставленный',
        'order_waiting': 'Ожидающий',
        'order_canceled': 'Отменено',
        'order_count': 'Количество заказов',
        'order_address': 'адрес заказа',
        'count_unit': 'шт',
        'enter_name': 'Пожалуйста, введите ваше имя',
        'enter_firstname': 'Пожалуйста, введите ваше имя',
        'enter_lastname': 'Пожалуйста, введите свою фамилию',
        'enter_address': 'Пожалуйста, введите ваш адрес',
        'enter_phone': 'Пожалуйста, введите свой номер телефона',
        'enter_password': 'Пожалуйста введите ваш пароль',
        'enter_message': 'Пожалуйста, введите ваше сообщение',
        'add_login_details': 'Добавьте свои данные для входа',
        'have_an_account': 'Уже есть аккаунт?',
        'start_order': 'Начать заказ',
        'new_product': 'Новый товар добавлен в корзину',
        'new_order': 'Вы сделали новый заказ',
        'success': 'Успешно',
        'failure': 'Отказ',
        'notifications': 'Уведомления',
        'added_to_cart': 'Добавлен в корзину',
        'view_cart': 'Посмотреть корзину',
        'copied': 'Скопировано'
      },
      'tm': {
        'home_page': 'Baş sahypa',
        'cart_page': 'Sebet',
        'liked_page': 'Halanlarym',
        'profile_page': 'Profil',
        'loading': 'Ýüklenýär',
        'delivery': 'Eltip Berme',
        'total': 'Jemi',
        'order': 'Sargyt et',
        'login': 'Giriş',
        'register': 'Registrasiýa',
        'no_register': 'Registrasiýasyz sargamak',
        'contact_info': 'Kontakt maglumatlary',
        'name': 'Ady',
        'firstname': 'Ady',
        'lastname': 'Familiýasy',
        'address': 'Salgysy',
        'phone': 'Tel',
        'continue': 'Dowam et',
        'order_success': 'Siziň sargydyňyz kabul edildi!',
        'delivery_will_contact':
            'Biziň kurýerymyz siziň bilen gysga wagtyň içinde habarlaşar',
        'ok': 'Bolýa',
        'delivery_address': 'Eltip berme salgysy',
        'change': 'Üýtgetmek',
        'payment_type': 'Töleg görnüşi',
        'pay_cash': 'Nagt töleg',
        'card_payment': 'Kart üsti töleg',
        'price': 'Umumy baha',
        'discount_food': 'Miwe we gök önümlerde uly arzanladyş',
        'delivery_only_dashoguz':
            'Eltip berme hyzmaty diňe Daşoguz welaýatynyň içinde amala aşyrylýar',
        'search': 'Gözle',
        'our_add_here': 'Mahabatyňyz şu ýerde',
        'new': 'TAZE',
        'day': 'gün',
        'hour': 'sag',
        'minute': 'min',
        'kg': 'kg',
        'add_to_cart': 'Sebede goş',
        'related_products': 'Bu haryt bilen alynyanlar',
        'add_new_address': 'Täze salgy goş',
        'input_info': 'Maglumatlaryňyzy giriziň',
        'save': 'Ýatda sakla',
        'my_addresses': 'Salgylarym',
        'contact_with_us': 'Biz bilen habarlaşyň',
        'enter_comment': 'Kommentariýaňyzy giriziň',
        'send': 'Ugrat',
        'change_address': 'Salgyny üýtgetmek',
        'enter_info': 'Maglumatlaryňyzy giriziň',
        'change_lang': 'Dilini çalşmak',
        'salam': 'Salam',
        'my_orders': 'Meniň sargytlarym',
        'logout': 'Çykmak',
        'main_address': 'Esasy salgy',
        'not_found': 'Tapylmady',
        'order_name': 'Sargyt',
        'ordered_by': 'Satyn alyjy',
        'order_delivered': 'Ýerine ýetirildi',
        'order_waiting': 'Garaşylýar',
        'order_canceled': 'Ýatyryldy',
        'order_count': 'Sargydyň sanawy',
        'order_address': 'Sargydyň salgysy',
        'count_unit': 'sany',
        'enter_name': 'Adyňyzy ýazmagyňyzy haýyş edýäris',
        'enter_firstname': 'Adyňyzy ýazmagyňyzy haýyş edýäris',
        'enter_lastname': 'Familiýaňyzy ýazmagyňyzy haýyş edýäris',
        'enter_address': 'Salgyňyzy ýazmagyňyzy haýyş edýäris',
        'enter_phone': 'Telefon belgisini giriziň',
        'enter_password': 'Parolyňyzy giriziň',
        'enter_message': 'Habaryňyzy ýazmagyňyzy haýyş edýäris',
        'add_login_details': 'Giriş maglumatlaryňyzy goşuň',
        'have_an_account': 'Hasabyňyz barmy?',
        'start_order': 'Sargyt başlaň',
        'new_product': 'Sebete täze önüm goşuldy',
        'new_order': 'Täze sargyt etdiň',
        'success': 'Üstünlik',
        'failure': 'Şowsuzlyk',
        'notifications': 'Duýduryşlar',
        'added_to_cart': 'Sebete goşuldy',
        'view_cart': 'Sebeti gör',
        'copied': 'Göçürildi'
      },
    };
    return ViewModel(
      markets: store.state.marketsState.markets,
      nots: store.state.userState.nots,
      marketCategories: store.state.marketCategoriesState.marketCategories,
      products: store.state.productsState.products,
      cartProducts: store.state.userState.cartProducts,
      addresses: store.state.userState.addresses,
      userAddresses: store.state.userState.userAddresses,
      orders: store.state.userState.orders,
      user: store.state.userState.user,
      isLogin: store.state.userState.user != null &&
          store.state.userState.user.id != null,
      favorites: store.state.userState.favorites,
      token: store.state.userState.user != null
          ? store.state.userState.user.token
          : null,
      lang: store.state.langState.lang,
      setUser: (UserModel user) async {
        return await store.dispatch(SetUserAction(user: user));
      },
      setNots: (NotificationModel not) async {
        List<NotificationModel> nots = store.state.userState.nots;
        NotificationModel notifi = not;
        if (nots == null) {
          nots = [];
        }
        Random random = new Random();
        int randomNumber = random.nextInt(1000000);
        notifi.id = randomNumber;
        nots.add(notifi);

        return await store.dispatch(SetUserAction(nots: nots));
      },
      addToCart: (ProductModel product, double amount) async {
        ProductModel pro = product;
        pro.amountInCart = amount;

        List<ProductModel> cartProducts = store.state.userState.cartProducts;

        if (cartProducts != null) {
          List<ProductModel> existProducts =
              cartProducts.where((element) => element.id == pro.id).toList();

          if (existProducts != null && existProducts.length > 0) {
            ProductModel exist = existProducts[0];

            if (exist != null && exist.id != null) {
              // cartProducts.removeWhere((element) => element.id == exist.id);
              exist.amountInCart = amount;
              // cartProducts.add(exist);
            } else {
              cartProducts.add(pro);
            }
          } else {
            cartProducts.add(pro);
          }
        }

        return store.dispatch(SetUserAction(cartProducts: cartProducts));
      },
      removeFromCart: (int id) async {
        List<ProductModel> cartProducts = store.state.userState.cartProducts;
        cartProducts.removeWhere((element) => element.id == id);
        return store.dispatch(SetUserAction(cartProducts: cartProducts));
      },
      emptyCart: () async {
        List<ProductModel> cartProducts = store.state.userState.cartProducts;
        cartProducts = [];
        return store.dispatch(SetUserAction(cartProducts: cartProducts));
      },
      fetchFavorites: (int id) async {
        List<ProductModel> favorites = await UserService.listFavorites(id);
        return store.dispatch(SetUserAction(favorites: favorites));
      },
      addToFavorites: (ProductModel pro) async {
        int userId;
        if (store.state.userState.user != null) {
          userId = store.state.userState.user.id;
        }
        List<ProductModel> favorites = store.state.userState.favorites;
        bool isExist = favorites.any((element) => element.id == pro.id);
        if (!isExist) {
          favorites.add(pro);
          if (userId != null) {
            await UserService.addToFavorites(userId, pro.id);
          }
        }
        return store.dispatch(SetUserAction(favorites: favorites));
      },
      removeFromFavorites: (ProductModel pro) async {
        int userId;
        if (store.state.userState.user != null) {
          userId = store.state.userState.user.id;
        }
        List<ProductModel> favorites = store.state.userState.favorites;
        bool isExist = favorites.any((element) => element.id == pro.id);
        if (isExist) {
          favorites.removeWhere((element) => element.id == pro.id);
          if (userId != null) {
            await UserService.removeFromFavorites(userId, pro.id);
          }
        }
        return store.dispatch(SetUserAction(favorites: favorites));
      },
      fetchMarkets: () async {
        List<MarketModel> markets = await MarketService.list();
        return store.dispatch(SetMarketsAction(markets: markets));
      },
      fetchMarketCategories: (int id) async {
        List<MarketCategoryModel> categories =
            await MarketService.listCategories(id);
        return store
            .dispatch(SetMarketCategoriesAction(marketCategories: categories));
      },
      fetchUserAddresses: () async {
        int id = store.state.userState.user.id;
        List<UserAddressModel> userAds = await UserService.getUserAddress(id);
        await store.dispatch(SetUserAction(userAddresses: userAds));
      },
      setUserAddresses: (List<UserAddressModel> userAds) async {
        await store.dispatch(SetUserAction(userAddresses: userAds));
      },
      setMarketCategories: (List<MarketCategoryModel> categories) async {
        await store
            .dispatch(SetMarketCategoriesAction(marketCategories: categories));
      },
      fetchProducts: (int id) async {
        List<ProductModel> products = await MarketService.listProducts(id);
        return store.dispatch(SetProductsAction(products: products));
      },
      setProducts: (List<ProductModel> products) async {
        return store.dispatch(SetProductsAction(products: products));
      },
      fetchAddresses: () async {},
      // fetchAddresses: () async {
      //   List<AddressModel> addresses = await UserService.listAddresses();
      //   return store.dispatch(SetUserAction(addresses: addresses));
      // },
      setOrders: (List<OrderModel> orders) async {
        return await store.dispatch(SetUserAction(orders: orders));
      },
      setLang: (String lang) async {
        return store.dispatch(SetLangAction(lang: lang));
      },
      getLangValue: (String value) {
        String _selectedLanguage = store.state.langState.lang;
        if (_languagesValues[_selectedLanguage] == null) return value;
        return _languagesValues[_selectedLanguage][value] ?? value;
      },
    );
  }
}
