import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  AppTheme._();
  static Color primary = HexColor('#CA7C00');
  static Color secondary = HexColor('#769E49');
  static Color appBarBg = HexColor('#F5F5F6');
  static Color appScreenBg = HexColor('#F5F5F6');
  static Color danger = HexColor('#E7655D');

  static Color black = HexColor('#485062');

  static Color linkTextColor = HexColor('#C52328');

  static Color hintTextColor = HexColor('#6E7FAA');
  static Color background = HexColor('#F7F8F0');
  static Color cardBackground = HexColor('#F0F0E7');

  static TextTheme _buildTextTheme(TextTheme base) {
    const String fontName = 'Poppins';
    return base.copyWith(
      button: base.button.copyWith(
        fontFamily: fontName,
        color: Colors.white,
      ),
      caption: base.caption.copyWith(fontFamily: fontName),
      overline: base.overline.copyWith(fontFamily: fontName),
      headline1: base.headline1.copyWith(fontFamily: fontName),
      headline2: base.headline2.copyWith(fontFamily: fontName),
      headline3: base.headline3.copyWith(fontFamily: fontName),
      headline4: base.headline4.copyWith(fontFamily: fontName),
      headline5: base.headline5.copyWith(fontFamily: fontName),
      headline6: base.headline6.copyWith(fontFamily: fontName),
      subtitle1: base.subtitle1.copyWith(fontFamily: fontName),
      subtitle2: base.subtitle2.copyWith(fontFamily: fontName),
      bodyText1: base.bodyText1.copyWith(fontFamily: fontName),
      bodyText2: base.bodyText2.copyWith(fontFamily: fontName),
    );
  }

  static ThemeData buildLightTheme() {
    final Color primary = HexColor('#54D3C2');
    final Color secondaryColor = HexColor('#54D3C2');
    final ColorScheme colorScheme = const ColorScheme.light().copyWith(
      primary: primary,
      secondary: secondaryColor,
    );
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      colorScheme: colorScheme,
      primaryColor: primary,
      buttonColor: primary,
      indicatorColor: Colors.white,
      splashColor: Colors.white24,
      splashFactory: InkRipple.splashFactory,
      accentColor: secondaryColor,
      canvasColor: Colors.white,
      backgroundColor: const Color(0xFFFFFFFF),
      scaffoldBackgroundColor: HexColor("#fcf6eb"),
      errorColor: const Color(0xFFB00020),
      buttonTheme: ButtonThemeData(
        colorScheme: colorScheme,
        textTheme: ButtonTextTheme.primary,
      ),
      textTheme: _buildTextTheme(base.textTheme),
      primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
      accentTextTheme: _buildTextTheme(base.accentTextTheme),
      platform: TargetPlatform.iOS,
      unselectedWidgetColor: HexColor('#EEEEEE'),
    );
  }

  static TextStyle hintTextStyle = TextStyle(
    color: HexColor('#b5c9e8'),
    fontWeight: FontWeight.w200,
  );

  static TextStyle screenTitleStyle = TextStyle(
    fontSize: 22,
    color: Colors.black,
  );

  static TextStyle subTitleStyle = TextStyle(
    fontSize: 16,
    color: HexColor('#3C3C3C'),
  );
}

ThemeData appTheme() {
  final double _appbarTitleFontSize = 18.0;

  return ThemeData(
    primaryColor: AppTheme.primary,
    primaryIconTheme: IconThemeData(
      color: AppTheme.primary,
    ),
    brightness: Brightness.light,
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(
        headline6: TextStyle(
          fontSize: _appbarTitleFontSize,
          color: AppTheme.primary,
          fontFamily: GoogleFonts.poppins().fontFamily,
        ),
      ),
      color: Colors.white,
      iconTheme: IconThemeData(
        color: AppTheme.primary,
      ),
      elevation: 0,
    ),
    inputDecorationTheme: InputDecorationTheme(
        fillColor: HexColor('#F2F2F2'),
        filled: true,
        errorMaxLines: 3,
        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(6)),
            borderSide: BorderSide.none),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(6)),
            borderSide: BorderSide.none),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: AppTheme.secondary,
            width: 1,
          ),
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        prefixStyle: TextStyle(
          color: Colors.grey,
        ),
        suffixStyle: TextStyle(
          color: HexColor('#b5c9e8'),
        ),
        focusColor: HexColor('#b5c9e8'),
        labelStyle: TextStyle(color: HexColor('#B6B7B7'))),
    textTheme: GoogleFonts.poppinsTextTheme(),
  );
}

Widget veritcalSpace(double height) {
  return SizedBox(height: height);
}

DropdownMenuItem dropdownItem({label, value}) {
  return DropdownMenuItem(
    value: value,
    child: Text(
      '$label',
      style: TextStyle(
        fontSize: 16,
        color: Colors.black,
      ),
    ),
  );
}
