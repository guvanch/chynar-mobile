import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/home/product_detail_screen.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/not_found.dart';
import 'package:chynar_mobile/widgets/product_cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class LikedScreen extends StatefulWidget {
  @override
  _LikedScreenState createState() => _LikedScreenState();
}

class _LikedScreenState extends State<LikedScreen> {
  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      appBar: AppBar(
        title: Text(vm.getLangValue('liked_page')),
        centerTitle: true,
      ),
      body: vm.favorites != null && vm.favorites.length > 0
          ? Container(
              child: ListView.builder(
                  itemCount: vm.favorites.length,
                  itemBuilder: (context, index) {
                    final product = vm.favorites[index];
                    return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductDetailScreen(
                                        product: product,
                                      )));
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                border: index != vm.favorites.length - 1
                                    ? Border(
                                        bottom: BorderSide(
                                            color: HexColor('#707070')
                                                .withOpacity(0.2)))
                                    : Border()),
                            padding: const EdgeInsets.all(8.0),
                            child: ProductCart(
                              product: product,
                              deletable: false,
                            )));
                  }))
          : Center(child: NotFound()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        if (vm.isLogin) {
          EasyLoading.show(status: '${vm.getLangValue('loading')}...');
          Future.delayed(new Duration(milliseconds: 1000), () async {
            await vm.fetchFavorites(vm.user.id);

            EasyLoading.dismiss();
          });
        }
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
