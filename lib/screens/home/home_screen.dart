import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/home/market_screen.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String fileUrl = Constants.fileServerUrl;

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 60, 20, 0),
            child: Stack(children: [
              SizedBox(
                width: double.infinity,
                height: 200,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset(
                    "assets/images/main_image.png",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 80, left: 20),
                child: SizedBox(
                  width: 180,
                  child: Text(
                    vm.getLangValue('discount_food'),
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: Color(0xffFDFBFA),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 210, left: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.location_on_sharp,
                      color: AppTheme.primary,
                    ),
                    Expanded(
                      child: Text(
                        vm.getLangValue('delivery_only_dashoguz'),
                        softWrap: true,
                        maxLines: 2,
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff0C0B0B)),
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
          vm.markets != null
              ? Expanded(
                  child: GridView.count(
                    crossAxisCount: 2,
                    childAspectRatio: 5 / 4,
                    children: List.generate(
                      vm.markets.length,
                      (index) {
                        final market = vm.markets[index];
                        return InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          MarketScreen(market: market)));
                            },
                            child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: Container(
                                    color: Colors.white,
                                    child: FadeInImage(
                                      image: NetworkImage(
                                          "$fileUrl${market.surat}"),
                                      placeholder: AssetImage(
                                          "assets/images/transparent.png"),
                                      fit: BoxFit.contain,
                                      imageErrorBuilder: (BuildContext context,
                                          Object exception,
                                          StackTrace stackTrace) {
                                        return Center(
                                            child: Text(
                                          vm.lang == 'en'
                                              ? market.nameEn
                                              : vm.lang == 'ru'
                                                  ? market.nameRu
                                                  : market.nameTm,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16),
                                        ));
                                      },
                                    ),
                                  ),
                                )));
                      },
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        EasyLoading.show(status: '${vm.getLangValue('loading')}...');
        Future.delayed(new Duration(milliseconds: 1000), () async {
          await vm.fetchMarkets();
          EasyLoading.dismiss();
        });
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
