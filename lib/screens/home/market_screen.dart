import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/market_category_model.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/services/market_service.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/grid_view.dart';
import 'package:chynar_mobile/widgets/not_found.dart';
import 'package:chynar_mobile/widgets/search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:url_launcher/url_launcher.dart';

class MarketScreen extends StatefulWidget {
  final MarketModel market;

  const MarketScreen({Key key, this.market}) : super(key: key);

  @override
  _MarketScreenState createState() => _MarketScreenState();
}

class _MarketScreenState extends State<MarketScreen> {
  String fileUrl = Constants.fileServerUrl;
  int _chosen = 0;
  final _controller = ScrollController();

  List<ProductModel> _filteredProducts = [];
  List<ProductModel> _products = [];

  @override
  initState() {
    super.initState();
  }

  Widget _render(BuildContext context, ViewModel vm) {
    var market = widget.market;
    return Scaffold(
      backgroundColor: HexColor('#fcf6eb'),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    flex: 4,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),
                              child: Container(
                                child: FadeInImage(
                                  image:
                                      NetworkImage("$fileUrl${market.surat}"),
                                  placeholder: AssetImage(
                                      "assets/images/transparent.png"),
                                  fit: BoxFit.cover,
                                  height: 40,
                                  width: 40,
                                  imageErrorBuilder: (BuildContext context,
                                      Object exception, StackTrace stackTrace) {
                                    return Container(
                                      width: 50,
                                      height: 50,
                                    );
                                  },
                                ),
                              )),
                        ),
                        Expanded(
                          child: Text(
                            vm.lang == 'en'
                                ? market.nameEn
                                : vm.lang == 'ru'
                                    ? market.nameRu
                                    : market.nameTm,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                                fontSize: 26,
                                color: AppTheme.primary,
                                height: 1),
                          ),
                        )
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            _showAlertDialog(context, vm);
                          },
                          child: Icon(
                            Icons
                                .notifications_active_sharp, //Icons.notifications_sharp,
                            color: AppTheme.secondary,
                            size: 20,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: InkWell(
                            onTap: () {
                              _openPhoneDialog(context, vm);
                            },
                            child: Icon(
                              Icons.phone,
                              color: AppTheme.secondary,
                              size: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: vm.getLangValue('search'),
                      prefixIcon: Icon(
                        Icons.search_sharp,
                        color: Colors.grey,
                      )),
                  autofocus: false,
                  readOnly: true,
                  onTap: () {
                    showSearch(
                        context: context,
                        delegate: DataSearch(
                            products: _products, market: widget.market));
                  },
                ),
              ),
              Stack(children: [
                SizedBox(
                  width: double.infinity,
                  height: 200,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.asset(
                      "assets/images/main_image.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 100, left: 20),
                  child: SizedBox(
                    width: 180,
                    height: 50,
                    child: Text(
                      vm.getLangValue('our_add_here'),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        color: Color(0xffFDFBFA),
                      ),
                    ),
                  ),
                )
              ]),
              vm.marketCategories != null && vm.marketCategories.length > 0
                  ? Container(
                      height: 60,
                      child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        scrollDirection: Axis.horizontal,
                        controller: _controller,
                        itemCount: vm.marketCategories.length,
                        itemBuilder: (BuildContext context, int index) {
                          MarketCategoryModel category =
                              vm.marketCategories[index];
                          return InkWell(
                            onTap: () {
                              setState(() {
                                _chosen = category.id;
                              });
                              _animateToIndex(index);
                              _setProducts(category.id, _products);
                            },
                            child: Container(
                              height: 20,
                              padding: const EdgeInsets.symmetric(
                                  vertical: 4, horizontal: 14),
                              margin: const EdgeInsets.all(8),
                              decoration: _chosen == category.id
                                  ? BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppTheme.secondary,
                                    )
                                  : BoxDecoration(),
                              child: Center(
                                  child: Text(
                                vm.lang == 'en'
                                    ? category.nameEn
                                    : vm.lang == 'ru'
                                        ? category.nameRu
                                        : category.nameTm,
                                style: TextStyle(
                                    color: _chosen == category.id
                                        ? Colors.white
                                        : Colors.grey),
                              )),
                            ),
                          );
                        },
                      ),
                    )
                  : Container(),
              _filteredProducts != null && _filteredProducts.length > 0
                  ? CustomGrid(items: _filteredProducts)
                  : Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Container(
                        child: Column(
                          children: [
                            NotFound(),
                          ],
                        ),
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showAlertDialog(BuildContext context, ViewModel vm) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(vm.getLangValue('notifications')),
          content: SingleChildScrollView(
              child: vm.nots != null && vm.nots.length > 1
                  ? ListBody(
                      children: vm.nots
                          .map(
                            (e) => Row(children: [
                              Icon(
                                _getIcon(e.action),
                                color: AppTheme.secondary,
                                size: 20,
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    vm.getLangValue(e.action),
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              )
                            ]),
                          )
                          .toList())
                  : NotFound()),
          actions: <Widget>[
            TextButton(
              child: Text(vm.getLangValue('ok'),
                  style: TextStyle(color: Colors.grey)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  IconData _getIcon(String action) {
    if (action == 'new_product') {
      return Icons.shopping_cart_outlined;
    } else if (action == 'new_order') {
      return Icons.check;
    }

    return Icons.notifications_sharp;
  }

  Future<void> _openPhoneDialog(BuildContext context, ViewModel vm) async {
    var market = widget.market;
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(vm.lang == 'en'
              ? market.nameEn
              : vm.lang == 'ru'
                  ? market.nameRu
                  : market.nameTm),
          content: SingleChildScrollView(
            child: market.phoneNumbers != null && market.phoneNumbers.length > 0
                ? ListBody(
                    children: market.phoneNumbers
                        .map((e) => TextButton(
                              onPressed: () async {
                                try {
                                  await launch(
                                      "tel://${_removeSpace(e.phoneNumber)}");
                                } catch (e) {
                                  Clipboard.setData(ClipboardData(
                                      text: e.phoneNumber
                                          .replaceAll(new RegExp(r"\s+"), "")));
                                  EasyLoading.showSuccess(
                                      vm.getLangValue('copied'));
                                  print(e.toString());
                                }
                              },
                              child: Row(children: [
                                Flexible(
                                  flex: 1,
                                  child: Icon(
                                    Icons.phone,
                                    color: AppTheme.secondary,
                                    size: 20,
                                  ),
                                ),
                                Flexible(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 40.0),
                                    child: Text(
                                      e.phoneNumber,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ),
                                )
                              ]),
                            ))
                        .toList(),
                  )
                : NotFound(),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(vm.getLangValue('ok'),
                  style: TextStyle(color: Colors.grey)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  String _removeSpace(String value) {
    return value.replaceAll(new RegExp(r"\s+"), "").toString();
  }

  void _setProducts(int categoryId, List<ProductModel> products) {
    List<ProductModel> filtered = products
        .where((element) => (element.marketCategoryId == categoryId &&
            element.marketId == widget.market.id))
        .toList();
    setState(() {
      _filteredProducts = filtered;
    });
  }

  _animateToIndex(i) => _controller.animateTo(60.0 * i,
      duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        EasyLoading.show(status: '${vm.getLangValue('loading')}...');
        Future.delayed(new Duration(milliseconds: 1000), () async {
          await _fetchMarketCategories(vm, widget.market.id);
          await _fetchProducts(vm, widget.market.id);
          EasyLoading.dismiss();
        });
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }

  Future<void> _fetchMarketCategories(ViewModel vm, int id) async {
    List<MarketCategoryModel> marketCategories =
        await MarketService.listCategories(id);
    vm.setMarketCategories(marketCategories);
    if (marketCategories != null && marketCategories.length > 0)
      setState(() {
        _chosen = marketCategories[0].id;
      });
  }

  Future<void> _fetchProducts(ViewModel vm, int id) async {
    List<ProductModel> products = await MarketService.listProducts(id);
    vm.setProducts(products);
    setState(() {
      _products = products;
    });
    _setProducts(_chosen, products);
  }
}
