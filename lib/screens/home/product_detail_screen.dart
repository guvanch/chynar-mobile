import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/notification_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/unit_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/cart/cart_screen.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:chynar_mobile/widgets/count.dart';
import 'package:chynar_mobile/widgets/grid_view.dart';
import 'package:chynar_mobile/widgets/not_found.dart';
import 'package:chynar_mobile/widgets/snack_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ProductDetailScreen extends StatefulWidget {
  final ProductModel product;
  final MarketModel market;

  const ProductDetailScreen({Key key, this.product, this.market})
      : super(key: key);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  String fileUrl = Constants.fileServerUrl;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  initState() {
    super.initState();
  }

  double _amount = 1;

  Widget _render(BuildContext context, ViewModel vm) {
    ProductModel product = widget.product;

    List<ProductModel> others = [];

    if (vm.products == null) {
      vm.fetchProducts(product.marketId);
    }

    others = vm.products
        .where((element) => element.id != widget.product.id)
        .toList();

    if (product.unit == null) {
      product.unit = new UnitModel();
      product.unit.name = 'kg';
    }

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: HexColor('#fcf6eb'),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        centerTitle: false,
        title: Row(children: [
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.keyboard_arrow_left_outlined,
              color: Colors.black,
              size: 36,
            ),
          ),
          product.isNew == true
              ? Container(
                  width: 50,
                  height: 20,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(14.0),
                  ),
                  child: Center(
                    child: Text(
                      vm.getLangValue('new'),
                      softWrap: true,
                      maxLines: 2,
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                          height: 1.5, fontSize: 12, color: Colors.white),
                    ),
                  ),
                )
              : Container(),
          product.isSale == true
              ? Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        width: 50,
                        height: 20,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(14),
                              bottomLeft: Radius.circular(14)),
                        ),
                        child: Center(
                          child: Text(
                            '11 ${vm.getLangValue('day')}',
                            softWrap: true,
                            maxLines: 2,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                                height: 1.5, fontSize: 12, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 2.0),
                      child: Container(
                        width: 50,
                        height: 20,
                        decoration: BoxDecoration(color: Colors.red),
                        child: Center(
                          child: Text(
                            '9 ${vm.getLangValue('hour')}',
                            softWrap: true,
                            maxLines: 2,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                                height: 1.5, fontSize: 12, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 50,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(14),
                            bottomRight: Radius.circular(14)),
                      ),
                      child: Center(
                        child: Text(
                          '21 ${vm.getLangValue('minute')}',
                          softWrap: true,
                          maxLines: 2,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              height: 1.5, fontSize: 12, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              : Container(),
        ]),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Container(
                    color: Colors.white,
                    child: FadeInImage(
                      image: NetworkImage("$fileUrl${widget.product.image}"),
                      placeholder: AssetImage("assets/images/transparent.png"),
                      fit: BoxFit.contain,
                      height: 200,
                      width: double.infinity,
                      imageErrorBuilder: (BuildContext context,
                          Object exception, StackTrace stackTrace) {
                        return Container(
                          width: 50,
                          height: 50,
                        );
                      },
                    ),
                  )),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      vm.lang == 'en'
                          ? product.nameEn ?? ''
                          : vm.lang == 'ru'
                              ? product.nameRu ?? ''
                              : product.nameTm ?? '',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Icon(
                              Icons.visibility_outlined,
                              size: 16,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              product.viewCount.toString(),
                              style: TextStyle(fontSize: 18),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () {
                    if (_liked(vm, product)) {
                      vm.removeFromFavorites(product);
                    } else {
                      vm.addToFavorites(product);
                    }
                  },
                  child: Icon(
                    _liked(vm, product)
                        ? Icons.favorite_outlined
                        : Icons.favorite_border_outlined,
                    size: 20,
                    color: Colors.red,
                  ),
                )
              ],
            ),
            Text(
              vm.lang == 'en'
                  ? product.descriptionEn ?? ''
                  : vm.lang == 'ru'
                      ? product.descriptionRu ?? ''
                      : product.descriptionTm ?? '',
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  product.salePrice != null
                      ? Text(
                          '${product.salePrice.toString()} TMT/${product.unit != null ? product.unit.name : 'kg'}',
                          style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.black87),
                        )
                      : Container(),
                  Text(
                    '${product.price} TMT/${product.unit.name}',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: Colors.redAccent),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Count(
                      product: product,
                      amount: _amount,
                      hideable: false,
                      increment: () => _increment(),
                      decrement: () => _decrement(),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: AppButton(
                        title: vm.getLangValue('add_to_cart'),
                        onClick: () async {
                          await vm.addToCart(product, _amount);
                          NotificationModel not = new NotificationModel();
                          not.action = 'new_product';
                          await vm.setNots(not);
                          _showSnackBar();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  vm.getLangValue('related_products'),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            vm.products != null
                ? SingleChildScrollView(
                    child: Container(
                        width: double.infinity,
                        child: vm.products.length > 0
                            ? CustomGrid(items: others)
                            : Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  child: Column(
                                    children: [
                                      NotFound(),
                                    ],
                                  ),
                                ),
                              )),
                  )
                : Center(child: NotFound())
          ],
        ),
      ),
    );
  }

  bool _liked(ViewModel vm, ProductModel pro) {
    bool isExist = false;
    if (vm.favorites != null) {
      isExist = vm.favorites.any((element) => element.id == pro.id);
    }
    return isExist;
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        content: SnackBarContent(
          onClick: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => CartScreen()));
          },
        )));
  }

  void _increment() {
    setState(() {
      _amount += widget.product.unitId == 12 ? 1 : 0.5;
    });
  }

  void _decrement() {
    setState(() {
      _amount -= widget.product.unitId == 12 ? 1 : 0.5;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
