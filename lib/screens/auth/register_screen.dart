import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/new_user_model.dart';
import 'package:chynar_mobile/models/user_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/auth/login_screen.dart';
import 'package:chynar_mobile/screens/profile/profile_screen.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();

  String _firstName;
  String _lastName;
  String _phoneNumber;
  String _password;
  int _userTypeId = 1;
  String _address;

  void _onSubmit(ViewModel vm) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    try {
      EasyLoading.show(status: '${vm.getLangValue('loading')}...');
      NewUserModel newUser = await UserService.register(_firstName, _lastName,
          _phoneNumber, _password, _userTypeId, _address);
      if (newUser != null && newUser.phoneNumber != null) {
        UserModel user =
            await UserService.login(newUser.phoneNumber, _password);
        await _setUser(vm, user);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => ProfileScreen()));
      } else if (newUser.msg != null) {
        EasyLoading.showInfo(newUser.msg);
      } else {
        EasyLoading.showError('Something went wrong');
      }
    } catch (e) {
      EasyLoading.showError(e.toString());
    }
  }

  Future<void> _setUser(ViewModel vm, UserModel user) async {
    try {
      await storage.write(key: 'token', value: user.token);
      await vm.setUser(user);
      EasyLoading.showSuccess('Login Success');
    } catch (e) {
      await vm.setUser(user);
    }
  }

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              vm.getLangValue('register'),
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(
                                vm.getLangValue('add_login_details'),
                                style: TextStyle(
                                    fontSize: 16, color: HexColor('#7C7D7E')),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('firstname')),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return vm.getLangValue('enter_firstname');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _firstName = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('lastname')),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return vm.getLangValue('enter_lastname');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _lastName = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          maxLines: 2,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('address')),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return vm.getLangValue('enter_address');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _address = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('phone'),
                              prefix: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  '+993',
                                  style: TextStyle(fontSize: 16),
                                ),
                              )),
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                value.length != 8) {
                              return vm.getLangValue('enter_phone');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _phoneNumber = '993' + val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          obscureText: true,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('Password')),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return vm.getLangValue('enter_password');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _password = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: AppButton(
                          title: vm.getLangValue('register'),
                          onClick: () => _onSubmit(vm),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              vm.getLangValue('have_an_account'),
                              style: TextStyle(
                                  fontSize: 16, color: HexColor('#7C7D7E')),
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginScreen()));
                                },
                                child: Text(
                                  vm.getLangValue('login'),
                                  style: TextStyle(color: AppTheme.secondary),
                                ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
