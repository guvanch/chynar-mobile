import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/user_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/auth/register_screen.dart';
import 'package:chynar_mobile/screens/profile/profile_screen.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();

  String _phone;
  String _password;

  void _onSubmit(ViewModel vm) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    try {
      EasyLoading.show(status: '${vm.getLangValue('loading')}...');
      UserModel user = await UserService.login(_phone, _password);
      if (user.login == true && user.token != null) {
        await _setUser(vm, user);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => ProfileScreen()));
      } else if (user.login == false && user.msg != null) {
        EasyLoading.showInfo(user.msg);
      } else {
        EasyLoading.showError('Something went wrong');
      }
    } catch (e) {
      EasyLoading.showError(e.toString());
    }
  }

  Future<void> _setUser(ViewModel vm, UserModel user) async {
    vm.fetchFavorites(user.id);
    try {
      await storage.write(key: 'id', value: user.id.toString());
      await vm.setUser(user);
      EasyLoading.showSuccess(vm.getLangValue('success'));
    } catch (e) {
      await vm.setUser(user);
    }
  }

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              vm.getLangValue('login'),
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(
                                'Добавьте свои данные для входа',
                                style: TextStyle(
                                    fontSize: 16, color: HexColor('#7C7D7E')),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('phone'),
                              prefix: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  '+993',
                                  style: TextStyle(fontSize: 16),
                                ),
                              )),
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                value.length != 8) {
                              return vm.getLangValue('enter_phone');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _phone = '+993' + val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          autofocus: false,
                          obscureText: true,
                          decoration: InputDecoration(
                              labelText: vm.getLangValue('Password')),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return vm.getLangValue('enter_password');
                            }
                            return null;
                          },
                          onSaved: (val) {
                            setState(() {
                              _password = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: AppButton(
                          title: vm.getLangValue('login'),
                          onClick: () => _onSubmit(vm),
                        ),
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(top: 8.0),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.center,
                      //     children: [
                      //       Text(
                      //         'Забыли пароль?',
                      //         style: TextStyle(
                      //             fontSize: 16, color: HexColor('#7C7D7E')),
                      //       ),
                      //       TextButton(
                      //           onPressed: () {
                      //             Navigator.push(
                      //                 context,
                      //                 MaterialPageRoute(
                      //                     builder: (context) =>
                      //                         ForgetPasswordScreen()));
                      //           },
                      //           child: Text(
                      //             'Сброс пароля',
                      //             style: TextStyle(color: AppTheme.secondary),
                      //           ))
                      //     ],
                      //   ),
                      // ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Еще нету аккаунта?',
                              style: TextStyle(
                                  fontSize: 16, color: HexColor('#7C7D7E')),
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RegisterScreen()));
                                },
                                child: Text(
                                  'Зарегистрироваться',
                                  style: TextStyle(color: AppTheme.secondary),
                                ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
