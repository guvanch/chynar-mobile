import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/profile/add_address_screen.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/address.dart';
import 'package:chynar_mobile/widgets/not_found.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class AddressScreen extends StatefulWidget {
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      backgroundColor: HexColor('#fcf6eb'),
      appBar: AppBar(
        title: Text(vm.getLangValue('my_addresses')),
        centerTitle: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: vm.userAddresses != null
                ? ListView.builder(
                    itemCount: vm.userAddresses.length,
                    itemBuilder: (context, index) {
                      final address = vm.userAddresses[index];
                      return Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Address(
                              address: address,
                              onClick: () {
                                _toConfirm(address);
                              }));
                    })
                : Center(child: NotFound()),
          ),
          Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 40.0),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(width: 1, style: BorderStyle.none)),
              child: DottedBorder(
                  color: AppTheme.secondary,
                  strokeWidth: 1,
                  dashPattern: [10, 10, 10, 10],
                  child: TextButton(
                      onPressed: () {
                        _toAddAddress();
                      },
                      style: ButtonStyle(
                        overlayColor: MaterialStateColor.resolveWith(
                            (states) => Colors.white.withOpacity(0.4)),
                        minimumSize: MaterialStateProperty.resolveWith(
                            (states) => Size(double.infinity, 36)),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.transparent),
                      ),
                      child: Text(
                        vm.getLangValue('add_new_address'),
                        style:
                            TextStyle(color: AppTheme.secondary, fontSize: 16),
                      )))),
        ],
      ),
    );
  }

  void _toConfirm(UserAddressModel address) {
    Navigator.pop(context, address);
  }

  void _toAddAddress() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => AddAddressScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        EasyLoading.show(status: '${vm.getLangValue('loading')}...');
        Future.delayed(new Duration(milliseconds: 1000), () async {
          if (vm.isLogin) {
            await vm.fetchUserAddresses();
          }

          EasyLoading.dismiss();
        });
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
