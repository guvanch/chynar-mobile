import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/radio_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key key}) : super(key: key);
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      appBar: AppBar(
        title: Text(vm.getLangValue('change_lang')),
        centerTitle: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
              child: Column(
                children: [
                  RadioButton(
                      title: 'Turkmen dili',
                      checked: vm.lang == 'tm',
                      border: 6,
                      onClick: () {
                        _setLang('tm', vm);
                      }),
                  SizedBox(height: 10),
                  RadioButton(
                      title: 'English',
                      checked: vm.lang == 'en',
                      border: 6,
                      onClick: () {
                        _setLang('en', vm);
                      }),
                  SizedBox(height: 10),
                  RadioButton(
                      title: 'Pусский',
                      checked: vm.lang == 'ru',
                      border: 6,
                      onClick: () {
                        _setLang('ru', vm);
                      })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _setLang(String lang, ViewModel vm) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('lang', lang);
      vm.setLang(lang);
    } catch (e) {
      print(e.toString());
      vm.setLang(lang);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
