import 'package:chynar_mobile/models/user_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/auth/login_screen.dart';
import 'package:chynar_mobile/screens/auth/register_screen.dart';
import 'package:chynar_mobile/screens/profile/address_screen.dart';
import 'package:chynar_mobile/screens/profile/contact_screen..dart';
import 'package:chynar_mobile/screens/profile/language_screen.dart';
import 'package:chynar_mobile/screens/profile/order_screen.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/profile_action.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final storage = new FlutterSecureStorage();

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 60, 20, 10),
        child: vm.isLogin
            ? Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          '${vm.getLangValue('salam')}, ${vm.user.name}',
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 3.0,
                        ),
                        child: IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.mode_edit_outline_outlined,
                            )),
                      ),
                    ],
                  ),
                  Divider(),
                  ProfileAction(
                      title: vm.getLangValue('my_orders'),
                      icon: Icons.shopping_bag_outlined,
                      onClick: () => _toOrder()),
                  Divider(),
                  ProfileAction(
                      title: vm.getLangValue('my_addresses'),
                      icon: Icons.location_on_outlined,
                      onClick: () => _toAddress()),
                  Divider(),
                  ProfileAction(
                      title: vm.getLangValue('contact_with_us'),
                      icon: Icons.connect_without_contact_outlined,
                      onClick: () => _toContact()),
                  Divider(),
                  ProfileAction(
                      title: vm.getLangValue('change_lang'),
                      icon: Icons.public_outlined,
                      onClick: () => _toLanguage()),
                  Divider(),
                  ProfileAction(
                      title: vm.getLangValue('logout'),
                      icon: Icons.logout_outlined,
                      onClick: () async {
                        _logout(vm);
                      }),
                ],
              )
            : Column(
                children: [
                  ProfileAction(
                      title: vm.getLangValue('register'),
                      icon: Icons.login_outlined,
                      onClick: () => _toRegister()),
                  Divider(),
                  ProfileAction(
                      title: vm.getLangValue('login'),
                      icon: Icons.login_outlined,
                      onClick: () => _toLogin())
                ],
              ),
      ),
    );
  }

  void _logout(ViewModel vm) async {
    try {
      await storage.delete(key: 'id');
      UserModel empty = new UserModel();
      await vm.setUser(empty);
    } catch (e) {
      print(e.toString());
    }
  }

  void _toOrder() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => OrderScreen()));
  }

  void _toAddress() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => AddressScreen()));
  }

  void _toContact() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ContactScreen()));
  }

  void _toLanguage() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LanguageScreen()));
  }

  void _toLogin() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  void _toRegister() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => RegisterScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {},
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
