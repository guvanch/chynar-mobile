import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/profile/profile_screen.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key key}) : super(key: key);
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  final _formKey = GlobalKey<FormState>();

  String _message;

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      appBar: AppBar(
        title: Text(vm.getLangValue('contact_with_us')),
        centerTitle: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
              child: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            vm.getLangValue('enter_comment'),
                            style: TextStyle(fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: TextFormField(
                        autofocus: false,
                        maxLines: 10,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return vm.getLangValue('enter_message');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _message = val;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(
                top: 10.0, bottom: 10, left: 30, right: 30),
            decoration: BoxDecoration(color: Colors.white, boxShadow: []),
            child: AppButton(
              title: vm.getLangValue('send'),
              onClick: () {
                _onSubmit(vm);
              },
            ),
          ),
        ],
      ),
    );
  }

  void _onSubmit(ViewModel vm) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    _sendMessage(vm);
  }

  _sendMessage(ViewModel vm) async {
    EasyLoading.show(status: '${vm.getLangValue('loading')}...');
    String username = 'bbatyr3110@gmail.com';
    String password = 'bl003277';

    final smtpServer = gmail(username, password);

    final message = Message()
      ..from = Address(username, 'Batyr')
      ..recipients.add('bbatyr3110@gmail.com')
      ..subject =
          'New Message from Chynar app: username: ${vm.user.name}, userid: ${vm.user.id} :: 😀 :: ${DateTime.now()}'
      ..text = _message;

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
      EasyLoading.showSuccess(vm.getLangValue('success'));
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => ProfileScreen()));
    } on MailerException catch (e) {
      EasyLoading.showError(vm.getLangValue('failure'));
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
