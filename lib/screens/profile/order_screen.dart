import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/home/home_screen.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_extra_button.dart';
import 'package:chynar_mobile/widgets/order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      backgroundColor: HexColor('#fcf6eb'),
      appBar: AppBar(
        title: Text(vm.getLangValue('my_orders')),
        centerTitle: false,
      ),
      body: Container(
          child: Column(
        children: [
          Expanded(
            child: vm.orders != null && vm.orders.length > 0
                ? ListView.builder(
                    itemCount: vm.orders.length,
                    itemBuilder: (context, index) {
                      final order = vm.orders[index];
                      return Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Order(order: order, vm: vm));
                    })
                : Center(
                    child: Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: AppExtraButton(
                        title: vm.getLangValue('start_order'),
                        onClick: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeScreen()));
                        },
                      ),
                    ),
                  ),
          ),
        ],
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        EasyLoading.show(status: '${vm.getLangValue('loading')}...');
        Future.delayed(new Duration(milliseconds: 1000), () async {
          _fetchOrders(vm, vm.user.id);
          EasyLoading.dismiss();
        });
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }

  Future<void> _fetchOrders(ViewModel vm, int id) async {
    List<OrderModel> orders = await UserService.listOrders(id);
    await vm.setOrders(orders);
  }
}
