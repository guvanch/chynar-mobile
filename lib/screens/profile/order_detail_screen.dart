import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class OrderDetailScreen extends StatefulWidget {
  final OrderModel order;

  const OrderDetailScreen({Key key, this.order}) : super(key: key);

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  Widget _render(BuildContext context, ViewModel vm) {
    OrderModel _order = widget.order;
    DateTime date = DateTime.parse(_order.updatedAt);

    return Scaffold(
        appBar: AppBar(
          title: Text(vm.getLangValue('order')),
          centerTitle: false,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.all(4),
                  padding: EdgeInsets.only(bottom: 10, right: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: Offset(0, 2)),
                    ],
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              color: HexColor('#EFF3F3'),
                              padding: const EdgeInsets.all(14.0),
                              child: Text(
                                vm.getLangValue('order_' + _order.status),
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: _order.status == 'delivered'
                                        ? AppTheme.secondary
                                        : _order.status == 'waiting'
                                            ? HexColor('#CC7D00')
                                            : Colors.red),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Text(
                                '${date.day}.${date.month}.${date.year}',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    fontSize: 12, color: HexColor('#BABABA')),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 14.0),
                            child: Text(
                              '${vm.getLangValue('order_name')} №',
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 14.0),
                            child: Text(
                              '#' + _order.id.toString(),
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: Row(
                  children: [
                    Text(
                      vm.getLangValue('order_address'),
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.all(4),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: Offset(0, 2)),
                    ],
                  ),
                  child: Column(children: [
                    Row(
                      children: [
                        Icon(
                          Icons.home_outlined,
                          color: AppTheme.primary,
                          size: 24,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(_order.user.lastname),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.account_circle_outlined,
                            color: AppTheme.primary,
                            size: 24,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(_order.user.firstname),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.phone,
                            color: AppTheme.primary,
                            size: 22,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(_order.user.phoneNumber),
                          )
                        ],
                      ),
                    ),
                  ])),
              Container(
                  margin: EdgeInsets.all(4),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: Offset(0, 2)),
                    ],
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 14.0),
                            child: Text(
                              vm.getLangValue('order_count'),
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 14.0),
                            child: Text(
                              '${_order.count} ${vm.getLangValue('count_unit')}',
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.all(4),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: Offset(0, 2)),
                    ],
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 14.0),
                            child: Text(
                              vm.getLangValue('price'),
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 14.0),
                            child: Text(
                              '${0} TMT',
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 14.0),
                            child: Text(
                              vm.getLangValue('delivery'),
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 14.0),
                            child: Text(
                              '${50} TMT',
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 14.0),
                            child: Text(
                              vm.getLangValue('total'),
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 14.0),
                            child: Text(
                              '${50} TMT',
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
