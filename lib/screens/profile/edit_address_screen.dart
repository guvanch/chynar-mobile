import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class EditAddressScreen extends StatefulWidget {
  final UserAddressModel address;

  const EditAddressScreen({Key key, this.address}) : super(key: key);
  @override
  _EditAddressScreenState createState() => _EditAddressScreenState();
}

class _EditAddressScreenState extends State<EditAddressScreen> {
  final _formKey = GlobalKey<FormState>();

  String _recName;
  String _recAddress;
  String _recNumber;
  @override
  void initState() {
    super.initState();
    _recName = widget.address.recName;
    _recAddress = widget.address.recAddress;
    _recNumber =
        widget.address.recNumber.substring(3, widget.address.recNumber.length);
  }

  void _onSubmit(ViewModel vm) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    try {
      await UserService.updateAddress(
          widget.address.id, _recName, _recAddress, _recNumber);
      EasyLoading.showSuccess(vm.getLangValue('success'));
      await vm.fetchUserAddresses();
      Navigator.pop(context);
    } catch (e) {
      EasyLoading.showError(e.toString());
    }
  }

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      appBar: AppBar(
        title: Text(vm.getLangValue('change_address')),
        centerTitle: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
              child: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            vm.getLangValue('enter_info'),
                            style: TextStyle(fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        autofocus: false,
                        initialValue: _recName,
                        decoration:
                            InputDecoration(labelText: vm.getLangValue('name')),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return vm.getLangValue('enter_name');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _recName = val;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        initialValue: _recAddress,
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: vm.getLangValue('address')),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return vm.getLangValue('enter_address');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _recAddress = val;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        autofocus: false,
                        initialValue: _recNumber,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: vm.getLangValue('phone'),
                            prefix: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text(
                                '+993',
                                style: TextStyle(fontSize: 16),
                              ),
                            )),
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              value.length != 8) {
                            return vm.getLangValue('enter_phone');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _recNumber = '+993' + val;
                          });
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 40.0),
            child: AppButton(
              title: vm.getLangValue('save'),
              onClick: () => _onSubmit(vm),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
