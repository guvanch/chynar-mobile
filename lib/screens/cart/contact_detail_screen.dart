import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/auth/login_screen.dart';
import 'package:chynar_mobile/screens/auth/register_screen.dart';
import 'package:chynar_mobile/screens/cart/order_confirm_screen.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:chynar_mobile/widgets/app_extra_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ContactDetailScreen extends StatefulWidget {
  final double total;

  const ContactDetailScreen({Key key, this.total}) : super(key: key);

  @override
  _ContactDetailScreenState createState() => _ContactDetailScreenState();
}

class _ContactDetailScreenState extends State<ContactDetailScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _recName = TextEditingController();
  TextEditingController _recAddress = TextEditingController();
  TextEditingController _recNumber = TextEditingController();

  void _onSubmit(ViewModel vm) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    String recNumber = '+993' + _recNumber.text;

    UserAddressModel address = new UserAddressModel();
    address.recAddress = _recAddress.text;
    address.recName = _recName.text;
    address.recNumber = recNumber;

    try {
      EasyLoading.show(status: '${vm.getLangValue('loading')}...');
      if (!vm.isLogin) {
        UserAddressModel userAddressModel =
            await UserService.createAddressNoUser(
                address.recName, address.recAddress, address.recNumber);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OrderConfirmScreen(
                    total: widget.total, address: userAddressModel)));
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    OrderConfirmScreen(total: widget.total, address: address)));
      }
      EasyLoading.dismiss();
    } catch (e) {
      EasyLoading.showError(e.toString());
    }
  }

  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      appBar: AppBar(
        title: Text(vm.getLangValue('order')),
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
              child: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    !vm.isLogin
                        ? Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: AppExtraButton(
                                    title: vm.getLangValue('login'),
                                    onClick: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LoginScreen()));
                                    },
                                  ),
                                ),
                              ),
                              Expanded(
                                  child: Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: AppExtraButton(
                                  title: vm.getLangValue('register'),
                                  onClick: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RegisterScreen()));
                                  },
                                ),
                              )),
                            ],
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: AppButton(
                        title: vm.getLangValue('no_register'),
                        onClick: () {},
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            vm.getLangValue('contact_info'),
                            style: TextStyle(fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        autofocus: false,
                        controller: _recName,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return vm.getLangValue('enter_name');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _recName.text = val;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: vm.getLangValue('name'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: _recAddress,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return vm.getLangValue('enter_address');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _recAddress.text = val;
                          });
                        },
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: vm.getLangValue('address')),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        autofocus: false,
                        controller: _recNumber,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              value.length != 8) {
                            return vm.getLangValue('enter_phone');
                          }
                          return null;
                        },
                        onSaved: (val) {
                          setState(() {
                            _recNumber.text = val;
                          });
                        },
                        decoration: InputDecoration(
                            labelText: vm.getLangValue('phone'),
                            prefix: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text(
                                '+993',
                                style: TextStyle(fontSize: 16),
                              ),
                            )),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 40.0),
            child: AppButton(
              title: vm.getLangValue('continue'),
              onClick: () => _onSubmit(vm),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      onInitialBuild: (vm) async {
        if (vm.isLogin) {
          EasyLoading.show(status: '${vm.getLangValue('loading')}...');
          Future.delayed(new Duration(milliseconds: 1000), () async {
            await _fetchUserAddress(vm);
            EasyLoading.dismiss();
          });
        }
      },
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }

  Future<void> _fetchUserAddress(ViewModel vm) async {
    List<UserAddressModel> userAddresses =
        await UserService.getUserAddress(vm.user.id);
    if (userAddresses != null && userAddresses.length > 0) {
      setState(() {
        _recName.text = userAddresses[0].recName;
        _recAddress.text = userAddresses[0].recAddress;
        _recNumber.text = userAddresses[0]
            .recNumber
            .substring(3, userAddresses[0].recNumber.length);
      });
    }
    await vm.setUserAddresses(userAddresses);
  }
}
