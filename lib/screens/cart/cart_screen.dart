import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/cart/contact_detail_screen.dart';
import 'package:chynar_mobile/screens/home/home_screen.dart';
import 'package:chynar_mobile/screens/home/product_detail_screen.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:chynar_mobile/widgets/app_extra_button.dart';
import 'package:chynar_mobile/widgets/product_cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  Widget _render(BuildContext context, ViewModel vm) {
    return Scaffold(
      appBar: AppBar(
        title: Text(vm.getLangValue('cart_page')),
        centerTitle: true,
      ),
      body: vm.cartProducts.length > 0
          ? Column(
              children: [
                Expanded(
                    child: Container(
                        child: ListView.builder(
                  itemCount: vm.cartProducts.length,
                  itemBuilder: (context, index) {
                    final product = vm.cartProducts[index];
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductDetailScreen(
                                      product: product,
                                    )));
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              border: index != vm.cartProducts.length - 1
                                  ? Border(
                                      bottom: BorderSide(
                                          color: HexColor('#707070')
                                              .withOpacity(0.2)))
                                  : Border()),
                          padding: const EdgeInsets.all(8.0),
                          child: ProductCart(
                            product: product,
                            deletable: true,
                          )),
                    );
                  },
                ))),
                Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 40.0),
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 5,
                        blurRadius: 3,
                        offset: Offset(3.0, 0))
                  ]),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            vm.getLangValue('delivery'),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '10 TMT',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              vm.getLangValue('total'),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${_getTotal(vm) + 10} TMT',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      AppButton(
                        title: vm.getLangValue('order'),
                        onClick: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ContactDetailScreen(
                                        total: _getTotal(vm),
                                      )));
                        },
                      ),
                    ],
                  ),
                ),
              ],
            )
          : Center(
              child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0),
              child: AppExtraButton(
                  title: vm.getLangValue('search'),
                  onClick: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  }),
            )),
    );
  }

  double _getTotal(vm) {
    double total = 0;
    for (int i = 0; i < vm.cartProducts.length; i++) {
      var cost = vm.cartProducts[i].amountInCart * vm.cartProducts[i].price;
      total += cost;
    }

    return total;
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
