import 'package:chynar_mobile/app_theme.dart';
import 'package:chynar_mobile/models/new_order_model.dart';
import 'package:chynar_mobile/models/notification_model.dart';
import 'package:chynar_mobile/models/ordered__product_modell.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/screens/cart/order_complete_screen.dart';
import 'package:chynar_mobile/screens/profile/address_screen.dart';
import 'package:chynar_mobile/services/user_service.dart';
import 'package:chynar_mobile/utils/hex_color.dart';
import 'package:chynar_mobile/view_model/view_model.dart';
import 'package:chynar_mobile/widgets/app_button.dart';
import 'package:chynar_mobile/widgets/radio_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';

class OrderConfirmScreen extends StatefulWidget {
  final double total;
  final UserAddressModel address;

  const OrderConfirmScreen({Key key, this.total, this.address})
      : super(key: key);

  @override
  _OrderConfirmScreenState createState() => _OrderConfirmScreenState();
}

class _OrderConfirmScreenState extends State<OrderConfirmScreen> {
  bool _payCash = false;
  UserAddressModel _address;

  Widget _render(BuildContext context, ViewModel vm) {
    if (_address == null) {
      _address = widget.address;
    }
    return Scaffold(
      appBar: AppBar(title: Text(vm.getLangValue('order'))),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 0),
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(vm.getLangValue('delivery_address')),
                              Text(
                                _address.recAddress,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                          TextButton(
                              onPressed: () async {
                                if (vm.isLogin) {
                                  var result = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AddressScreen()));
                                  if (result is UserAddressModel) {
                                    setState(() {
                                      _address = result;
                                    });
                                  }
                                } else {
                                  Navigator.pop(context);
                                }
                              },
                              child: Text(
                                vm.getLangValue('change'),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: AppTheme.primary,
                                    fontWeight: FontWeight.bold),
                              ))
                        ],
                      ),
                    ),
                    Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20),
                        color: HexColor('#F6F6F6'),
                        child: ListView.builder(
                          itemCount: vm.cartProducts.length,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemBuilder: (context, index) {
                            final product = vm.cartProducts[index];
                            return Container(
                                decoration: BoxDecoration(
                                  border: index != vm.cartProducts.length - 1
                                      ? Border(
                                          bottom: BorderSide(
                                              width: 0.5,
                                              color: HexColor('#707070')
                                                  .withOpacity(0.5)))
                                      : Border(),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 14.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        product.nameEn,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        '${product.amountInCart * product.price} TMT',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ],
                                  ),
                                ));
                          },
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                vm.getLangValue('payment_type'),
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                          SizedBox(height: 10),
                          Column(
                            children: [
                              RadioButton(
                                  title: vm.getLangValue('pay_cash'),
                                  checked: _payCash,
                                  border: 30.0,
                                  onClick: () {
                                    setState(() {
                                      _payCash = true;
                                    });
                                  }),
                              SizedBox(height: 10),
                              RadioButton(
                                  title: vm.getLangValue('card_payment'),
                                  checked: !_payCash,
                                  border: 30.0,
                                  onClick: () {
                                    setState(() {
                                      _payCash = false;
                                    });
                                  })
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(vm.getLangValue('price')),
                                Text(
                                  '${widget.total} TMT',
                                  style: TextStyle(
                                      color: AppTheme.primary,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(vm.getLangValue('delivery')),
                                Text(
                                  '10 TMT',
                                  style: TextStyle(
                                      color: AppTheme.primary,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
          Container(
            color: Colors.white,
            padding:
                const EdgeInsets.only(bottom: 10.0, left: 20.0, right: 20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(vm.getLangValue(vm.getLangValue('total'))),
                      Text(
                        '${widget.total + 10} TMT',
                        style: TextStyle(
                            color: AppTheme.primary,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                AppButton(
                  title: vm.getLangValue('order'),
                  onClick: () async {
                    await _onConfirm(vm);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _onConfirm(ViewModel vm) async {
    final df = new DateFormat('yyyy-MM-dd');
    var now = DateTime.now().millisecondsSinceEpoch;
    String orderTime =
        df.format(new DateTime.fromMillisecondsSinceEpoch(now * 1000));

    int sum = vm.cartProducts.length;

    NewOrderModel newOrder = new NewOrderModel();
    newOrder.sum = sum;
    newOrder.isCash = _payCash;
    newOrder.orderDateTime = orderTime;
    newOrder.deliveryDateTime = orderTime;
    newOrder.userId = vm.user != null ? vm.user.id : null;

    List<OrderedProductModel> orderedPros = [];
    vm.cartProducts.forEach((element) {
      OrderedProductModel orderPro = new OrderedProductModel();
      orderPro.amount = element.amountInCart;
      orderPro.delivered = false;
      orderPro.productId = element.id;
      orderedPros.add(orderPro);
    });
    newOrder.orderedProducts = orderedPros;

    var res = await UserService.createOrder(newOrder);
    EasyLoading.showInfo(res["msg"].toString());
    if (res["msg"] == "Successfully!") {
      await vm.emptyCart();
      EasyLoading.show(status: '${vm.getLangValue('loading')}...');
      NotificationModel not = new NotificationModel();
      not.action = 'new_order';
      await vm.setNots(not);
      Future.delayed(new Duration(milliseconds: 500), () async {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => OrderCompleteScreen()));
        EasyLoading.dismiss();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ViewModel>(
      distinct: false,
      converter: (store) => ViewModel.fromStore(store),
      builder: (BuildContext context, ViewModel vm) {
        return _render(context, vm);
      },
    );
  }
}
