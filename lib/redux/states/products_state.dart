import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:meta/meta.dart';

@immutable
class ProductsState {
  final List<ProductModel> products;

  ProductsState({this.products});

  factory ProductsState.initial() {
    return ProductsState(products: null);
  }

  static ProductsState fromJson(dynamic json) {
    return ProductsState(products: json['products']);
  }

  dynamic toJson() {
    return {'products': products};
  }

  ProductsState copyWith(
      {List<MarketModel> markets, List<ProductModel> products}) {
    return ProductsState(products: products ?? this.products);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProductsState &&
          runtimeType == other.runtimeType &&
          products == other.products;

  @override
  int get hashCode => products.hashCode;
}
