import 'package:chynar_mobile/models/profile_model.dart';
import 'package:meta/meta.dart';

@immutable
class AuthState {
  final String token;
  final String userId;
  final String email;
  final ProfileModel profile;

  AuthState({
    this.token,
    this.userId,
    this.email,
    this.profile,
  });

  factory AuthState.initial() {
    return AuthState(
      token: null,
      userId: null,
      email: null,
      profile: null,
    );
  }

  static AuthState fromJson(dynamic json) {
    return AuthState(
      token: json['token'],
      userId: json['userId'],
      email: json['email'],
    );
  }

  dynamic toJson() {
    return {'userId': userId, 'token': token, 'email': email};
  }

  AuthState copyWith({
    String token,
    String userId,
    String email,
    ProfileModel profile,
  }) {
    return AuthState(
      token: token ?? this.token,
      userId: userId ?? this.userId,
      email: email ?? this.email,
      profile: profile ?? this.profile,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AuthState &&
          runtimeType == other.runtimeType &&
          profile == other.profile &&
          email == other.email &&
          token == other.token &&
          userId == other.userId;

  @override
  int get hashCode =>
      token.hashCode ^ userId.hashCode ^ email.hashCode ^ profile.hashCode;
}
