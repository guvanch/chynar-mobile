import 'package:chynar_mobile/models/market_category_model.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:meta/meta.dart';

@immutable
class MarketCategoriesState {
  final List<MarketCategoryModel> marketCategories;

  MarketCategoriesState({this.marketCategories});

  factory MarketCategoriesState.initial() {
    return MarketCategoriesState(marketCategories: null);
  }

  static MarketCategoriesState fromJson(dynamic json) {
    return MarketCategoriesState(marketCategories: json['marketCategories']);
  }

  dynamic toJson() {
    return {'marketCategories': marketCategories};
  }

  MarketCategoriesState copyWith(
      {List<MarketModel> markets, List<MarketCategoryModel> marketCategories}) {
    return MarketCategoriesState(
        marketCategories: marketCategories ?? this.marketCategories);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MarketCategoriesState &&
          runtimeType == other.runtimeType &&
          marketCategories == other.marketCategories;

  @override
  int get hashCode => marketCategories.hashCode;
}
