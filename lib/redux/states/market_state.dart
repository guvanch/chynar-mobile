import 'package:chynar_mobile/models/market_model.dart';
import 'package:meta/meta.dart';

@immutable
class MarketsState {
  final List<MarketModel> markets;

  MarketsState({this.markets});

  factory MarketsState.initial() {
    return MarketsState(markets: null);
  }

  static MarketsState fromJson(dynamic json) {
    return MarketsState(markets: json['markets']);
  }

  dynamic toJson() {
    return {'markets': markets};
  }

  MarketsState copyWith({List<MarketModel> markets}) {
    return MarketsState(markets: markets ?? this.markets);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MarketsState &&
          runtimeType == other.runtimeType &&
          markets == other.markets;

  @override
  int get hashCode => markets.hashCode;
}
