import 'package:chynar_mobile/models/address_model.dart';
import 'package:chynar_mobile/models/notification_model.dart';
import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/models/user_model.dart';
import 'package:meta/meta.dart';

@immutable
class UserState {
  final UserModel user;
  final List<UserAddressModel> userAddresses;
  final List<AddressModel> addresses;
  final List<ProductModel> favorites;
  final List<NotificationModel> nots;
  final List<ProductModel> cartProducts;
  final List<OrderModel> orders;

  UserState(
      {this.addresses,
      this.favorites,
      this.cartProducts,
      this.orders,
      this.nots,
      this.user,
      this.userAddresses});

  factory UserState.initial() {
    return UserState(
        addresses: null,
        favorites: [],
        cartProducts: [],
        user: null,
        userAddresses: null,
        nots: null,
        orders: null);
  }

  static UserState fromJson(dynamic json) {
    return UserState(
        addresses: json['addresses'],
        user: json['user'],
        orders: json['orders'],
        userAddresses: json['userAddresses'],
        favorites: json['favorites'],
        nots: json['nots'],
        cartProducts: json['cartProducts']);
  }

  dynamic toJson() {
    return {
      'addresses': addresses,
      'user': user,
      'orders': orders,
      'userAddresses': userAddresses,
      'favorites': favorites,
      'nots': nots,
      'cartProducts': cartProducts
    };
  }

  UserState copyWith(
      {List<AddressModel> addresses,
      List<OrderModel> orders,
      List<ProductModel> favorites,
      List<ProductModel> cartProducts,
      List<UserAddressModel> userAddresses,
      List<NotificationModel> nots,
      UserModel user,
      String lang}) {
    return UserState(
        addresses: addresses ?? this.addresses,
        favorites: favorites ?? this.favorites,
        cartProducts: cartProducts ?? this.cartProducts,
        orders: orders ?? this.orders,
        userAddresses: userAddresses ?? this.userAddresses,
        nots: nots ?? this.nots,
        user: user ?? this.user);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserState &&
          runtimeType == other.runtimeType &&
          user == user &&
          addresses == other.addresses &&
          favorites == other.favorites &&
          cartProducts == other.cartProducts &&
          userAddresses == other.userAddresses &&
          nots == other.nots &&
          orders == other.orders;

  @override
  int get hashCode => addresses.hashCode;
}
