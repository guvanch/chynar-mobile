import 'package:meta/meta.dart';

@immutable
class LangState {
  final String lang;
  final Function getLangValue;

  LangState({this.lang, this.getLangValue});

  factory LangState.initial() {
    return LangState(lang: null, getLangValue: null);
  }

  static LangState fromJson(dynamic json) {
    return LangState(lang: json['lang']);
  }

  dynamic toJson() {
    return {'lang': lang, 'getLangValue': 'getLangValue'};
  }

  LangState copyWith({String langValue, String lang}) {
    return LangState(lang: lang ?? this.lang);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LangState &&
          runtimeType == other.runtimeType &&
          getLangValue == other.getLangValue;

  @override
  int get hashCode => lang.hashCode;
}
