import 'package:chynar_mobile/redux/states/auth_state.dart';
import 'package:chynar_mobile/redux/states/lang_state.dart';
import 'package:chynar_mobile/redux/states/market_categories_state.dart';
import 'package:chynar_mobile/redux/states/market_state.dart';
import 'package:chynar_mobile/redux/states/products_state.dart';
import 'package:chynar_mobile/redux/states/user_state.dart';
import 'package:flutter/material.dart';

@immutable
class AppState {
  final AuthState authState;
  final MarketsState marketsState;
  final MarketCategoriesState marketCategoriesState;
  final ProductsState productsState;
  final UserState userState;
  final LangState langState;

  AppState({
    @required this.authState,
    @required this.marketsState,
    @required this.marketCategoriesState,
    @required this.productsState,
    @required this.userState,
    @required this.langState,
  });

  factory AppState.initial() {
    return AppState(
      authState: AuthState.initial(),
      marketsState: MarketsState.initial(),
      marketCategoriesState: MarketCategoriesState.initial(),
      productsState: ProductsState.initial(),
      userState: UserState.initial(),
      langState: LangState.initial(),
    );
  }

  static AppState fromJson(dynamic json) {
    var x = AppState(
        authState: json != null && json['authState'] != null
            ? AuthState.fromJson(json['authState'])
            : AuthState.initial(),
        marketsState: MarketsState.initial(),
        marketCategoriesState: MarketCategoriesState.initial(),
        productsState: ProductsState.initial(),
        userState: UserState.initial(),
        langState: LangState.initial()
        // marketsState: json != null && json['marketsState'] != null
        //     ? MarketsState.fromJson(json['marketsState'])
        //     : MarketsState.initial(),
        );
    return x;
  }

  dynamic toJson() {
    return {
      'authState': authState,
      'marketsState': marketsState,
      'marketCategriesState': marketCategoriesState,
      'productsState': productsState,
      'userState': userState,
      'langState': langState,
    };
  }
}
