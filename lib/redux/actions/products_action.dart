import 'package:chynar_mobile/models/product_model.dart';
import 'package:meta/meta.dart';

class SetProductsAction {
  final List<ProductModel> products;

  SetProductsAction({@required this.products});

  @override
  String toString() {
    return "Setup Products";
  }
}
