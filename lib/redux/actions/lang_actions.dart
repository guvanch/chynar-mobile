class SetLangAction {
  final String lang;
  final Function getLangValue;

  SetLangAction({this.lang, this.getLangValue});

  @override
  String toString() {
    return "Setup lang action";
  }
}
