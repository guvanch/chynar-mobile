import 'package:chynar_mobile/models/address_model.dart';
import 'package:chynar_mobile/models/notification_model.dart';
import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/models/user_model.dart';

class SetUserAction {
  final UserModel user;
  final List<AddressModel> addresses;
  final List<UserAddressModel> userAddresses;
  final List<OrderModel> orders;
  final List<ProductModel> favorites;
  final List<NotificationModel> nots;
  final List<ProductModel> cartProducts;

  SetUserAction(
      {this.addresses,
      this.favorites,
      this.cartProducts,
      this.nots,
      this.orders,
      this.userAddresses,
      this.user});

  @override
  String toString() {
    return "Setup user action";
  }
}
