import 'package:chynar_mobile/models/profile_model.dart';
import 'package:flutter/material.dart';

class LoginSuccessAction {
  final String token;
  final String userId;
  final String email;

  LoginSuccessAction({
    @required this.token,
    @required this.userId,
    @required this.email,
  });

  @override
  String toString() {
    return "Login SUccess";
  }
}

class LogoutAction {
  LogoutAction();
}

class SetUserProfileAction {
  final ProfileModel profile;

  SetUserProfileAction({
    this.profile,
  });

  @override
  String toString() {
    return "Setup User Profile";
  }
}
