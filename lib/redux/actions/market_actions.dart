import 'package:chynar_mobile/models/market_model.dart';
import 'package:meta/meta.dart';

class SetMarketsAction {
  final List<MarketModel> markets;

  SetMarketsAction({@required this.markets});

  @override
  String toString() {
    return "Setup markets";
  }
}
