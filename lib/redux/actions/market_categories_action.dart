import 'package:chynar_mobile/models/market_category_model.dart';
import 'package:meta/meta.dart';

class SetMarketCategoriesAction {
  final List<MarketCategoryModel> marketCategories;

  SetMarketCategoriesAction({@required this.marketCategories});

  @override
  String toString() {
    return "Setup Market Categories";
  }
}
