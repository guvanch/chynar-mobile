import 'package:chynar_mobile/redux/actions/products_action.dart';
import 'package:chynar_mobile/redux/states/products_state.dart';
import 'package:redux/redux.dart';

Reducer<ProductsState> productsReducer = combineReducers([
  TypedReducer<ProductsState, SetProductsAction>(_setProductsAction),
]);

ProductsState _setProductsAction(
    ProductsState state, SetProductsAction action) {
  return state.copyWith(products: action.products);
}
