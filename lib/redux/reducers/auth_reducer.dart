import 'package:chynar_mobile/redux/actions/auth_actions.dart';
import 'package:chynar_mobile/redux/states/auth_state.dart';
import 'package:redux/redux.dart';

Reducer<AuthState> authReducer = combineReducers([
  TypedReducer<AuthState, LoginSuccessAction>(_loginSuccessAction),
  TypedReducer<AuthState, LogoutAction>(_logoutAction),
  TypedReducer<AuthState, SetUserProfileAction>(_setUserProfileAction),
]);

AuthState _loginSuccessAction(AuthState state, LoginSuccessAction action) {
  return AuthState(
    token: action.token,
    userId: action.userId,
    email: action.email,
  );
}

AuthState _logoutAction(AuthState state, LogoutAction action) {
  return AuthState(
    token: null,
    userId: null,
    email: null,
  );
}

AuthState _setUserProfileAction(AuthState state, SetUserProfileAction action) {
  return state.copyWith(
    profile: action.profile,
  );
}
