import 'package:chynar_mobile/redux/actions/user_actions.dart';
import 'package:chynar_mobile/redux/states/user_state.dart';
import 'package:redux/redux.dart';

Reducer<UserState> userReducer = combineReducers([
  TypedReducer<UserState, SetUserAction>(_setUserAction),
  TypedReducer<UserState, SetUserAction>(_setUserAddressesAction),
  TypedReducer<UserState, SetUserAction>(_setUserOrdersAction),
  TypedReducer<UserState, SetUserAction>(_setFavoritesAction),
  TypedReducer<UserState, SetUserAction>(_setNotsAction),
]);

UserState _setUserAction(UserState state, SetUserAction action) {
  return state.copyWith(user: action.user);
}

UserState _setUserAddressesAction(UserState state, SetUserAction action) {
  return state.copyWith(userAddresses: action.userAddresses);
}

UserState _setUserOrdersAction(UserState state, SetUserAction action) {
  return state.copyWith(orders: action.orders);
}

UserState _setFavoritesAction(UserState state, SetUserAction action) {
  return state.copyWith(favorites: action.favorites);
}

UserState _setNotsAction(UserState state, SetUserAction action) {
  return state.copyWith(nots: action.nots);
}
