import 'package:chynar_mobile/redux/actions/lang_actions.dart';
import 'package:chynar_mobile/redux/states/lang_state.dart';
import 'package:redux/redux.dart';

Reducer<LangState> langReducer = combineReducers([
  TypedReducer<LangState, SetLangAction>(_setLangAction),
]);

LangState _setLangAction(LangState state, SetLangAction action) {
  return state.copyWith(lang: action.lang);
}
