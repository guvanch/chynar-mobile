import 'package:chynar_mobile/redux/actions/market_categories_action.dart';
import 'package:chynar_mobile/redux/states/market_categories_state.dart';
import 'package:redux/redux.dart';

Reducer<MarketCategoriesState> marketCategoriesReducer = combineReducers([
  TypedReducer<MarketCategoriesState, SetMarketCategoriesAction>(
      _setMarketCategoriesAction),
]);

MarketCategoriesState _setMarketCategoriesAction(
    MarketCategoriesState state, SetMarketCategoriesAction action) {
  return state.copyWith(marketCategories: action.marketCategories);
}
