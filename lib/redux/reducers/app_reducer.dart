import 'package:chynar_mobile/redux/reducers/auth_reducer.dart';
import 'package:chynar_mobile/redux/reducers/lang_reducer.dart';
import 'package:chynar_mobile/redux/reducers/market_categories_reducer.dart';
import 'package:chynar_mobile/redux/reducers/market_reducer.dart';
import 'package:chynar_mobile/redux/reducers/products_reducer.dart';
import 'package:chynar_mobile/redux/reducers/user_reducer.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';

AppState appReducer(AppState state, dynamic action) {
  return AppState(
      authState: authReducer(state.authState, action),
      marketsState: marketReducer(state.marketsState, action),
      marketCategoriesState:
          marketCategoriesReducer(state.marketCategoriesState, action),
      productsState: productsReducer(state.productsState, action),
      userState: userReducer(state.userState, action),
      langState: langReducer(state.langState, action));
}
