import 'package:chynar_mobile/redux/actions/market_actions.dart';
import 'package:chynar_mobile/redux/states/market_state.dart';
import 'package:redux/redux.dart';

Reducer<MarketsState> marketReducer = combineReducers([
  TypedReducer<MarketsState, SetMarketsAction>(_setMarketsAction),
]);

MarketsState _setMarketsAction(MarketsState state, SetMarketsAction action) {
  return state.copyWith(markets: action.markets);
}
