import 'package:chynar_mobile/redux/reducers/app_reducer.dart';
import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';

import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

import 'package:redux_thunk/redux_thunk.dart';

Future<Store<AppState>> createStore() async {
  final persistor = Persistor<AppState>(
    storage: FlutterStorage(location: FlutterSaveLocation.sharedPreferences),
    serializer: JsonSerializer<AppState>(AppState.fromJson),
  );

  final initialState = await persistor.load();
  final persistorMiddleware = persistor.createMiddleware();

  return Store(
    appReducer,
    initialState: initialState ?? AppState.initial(),
    distinct: true,
    middleware: [
      thunkMiddleware,
      persistorMiddleware,
      LoggingMiddleware.printer(),
    ],
  );
}

var appStore;

createAppStore() async {
  appStore = await createStore();
  return appStore;
}
