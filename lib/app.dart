import 'package:chynar_mobile/redux/states/app_state.dart';
import 'package:chynar_mobile/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'app_theme.dart';

class App extends StatefulWidget {
  final Store<AppState> store;

  App({
    @required this.store,
  });

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: widget.store,
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: appTheme(),
            home: SplashScreen(),
            builder: EasyLoading.init()),
      ),
    );
  }
}
