import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/redux/store.dart';
import 'package:dio/dio.dart';

Dio createDio() {
  var token = appStore.state.authState.token;
  Dio dio = Dio(
    BaseOptions(
      connectTimeout: 5000,
      receiveTimeout: 5000,
      baseUrl: Constants.apiUrl,
      headers: {
        "Authorization": token,
      },
    ),
  );

  return dio;
}
