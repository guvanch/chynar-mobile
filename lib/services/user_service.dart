import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/new_order_model.dart';
import 'package:chynar_mobile/models/new_user_model.dart';
import 'package:chynar_mobile/models/order_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/profile_model.dart';
import 'package:chynar_mobile/models/user_address_model.dart';
import 'package:chynar_mobile/models/user_model.dart';
import 'package:chynar_mobile/redux/store.dart';
import 'package:chynar_mobile/utils/dio_util.dart';
import 'package:dio/dio.dart';

class UserService {
  UserService._();

  static Future<UserModel> login(String phone, String password) async {
    try {
      var formData = FormData.fromMap({
        'phoneNumber': phone,
        'password': password,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.post("$apiUrl/user/login", data: formData);
      UserModel data = UserModel.fromJson(res.data);
      return data;
    } catch (e) {
      print(">>>>>> post login error: ${e.toString()}");
      throw e;
    }
  }

  static Future<NewUserModel> register(
      String firstName,
      String lastName,
      String phoneNumber,
      String password,
      int userTypeId,
      String address) async {
    try {
      var formData = FormData.fromMap({
        'fname': firstName,
        'lastname': lastName,
        'phoneNumber': phoneNumber,
        'password': password,
        'UserTypeId': userTypeId,
        'rec_name': firstName,
        'rec_address': address,
        'rec_number': phoneNumber,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.post("$apiUrl/user/create", data: formData);
      NewUserModel data = NewUserModel.fromJson(res.data);
      return data;
    } catch (e) {
      print(">>>>>> post login error: ${e.toString()}");
      throw e;
    }
  }

  static Future<dynamic> createOrder(NewOrderModel order) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.post("$apiUrl/order/create", data: order);
      return res.data;
    } catch (e) {
      print(">>>>>> post order error: ${e.toString()}");
      throw e;
    }
  }

  static Future<List<ProductModel>> listFavorites(int userId) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/favourites/user/$userId");
      List<ProductModel> data = [];
      if (res.data != null) {
        res.data['rows'].forEach((v) {
          data.add(new ProductModel.fromJson(v['Product']));
        });
      }
      return data;
    } catch (e) {
      print(">>>>>> fetch favorites error: ${e.toString()}");
      throw e;
    }
  }

  static Future<void> addToFavorites(int userId, int productId) async {
    try {
      var formData = FormData.fromMap({
        'user_id': userId,
        'product_id': productId,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      await dio.post("$apiUrl/favourites/create", data: formData);
    } catch (e) {
      print(">>>>>> post favorite error: ${e.toString()}");
      throw e;
    }
  }

  static Future<void> removeFromFavorites(int userId, int productId) async {
    try {
      var formData = FormData.fromMap({
        'user_id': userId,
        'product_id': productId,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      await dio.post("$apiUrl/favourites/delete", data: formData);
    } catch (e) {
      print(">>>>>> delete favorite error: ${e.toString()}");
      throw e;
    }
  }

  static Future<List<UserAddressModel>> getUserAddress(int userId) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/user/address/$userId");
      List<UserAddressModel> userAddresses = [];

      if (res.data != null && res.data.length > 0) {
        res.data.forEach((v) {
          userAddresses.add(new UserAddressModel.fromJson(v));
        });
      }
      return userAddresses;
    } catch (e) {
      print(">>>>>> fetch user address error: ${e.toString()}");
      throw e;
    }
  }

  static Future<dynamic> createAddress(
      int userId, String recName, String recAddress, String recNumber) async {
    try {
      var formData = FormData.fromMap({
        'rec_name': recName,
        'rec_address': recAddress,
        'rec_number': recNumber,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res =
          await dio.post("$apiUrl/user/address/create/$userId", data: formData);
      return res.data;
    } catch (e) {
      print(">>>>>> post address error: ${e.toString()}");
      throw e;
    }
  }

  static Future<UserAddressModel> createAddressNoUser(
      String recName, String recAddress, String recNumber) async {
    try {
      var formData = FormData.fromMap({
        'rec_name': recName,
        'rec_address': recAddress,
        'rec_number': recNumber,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res =
          await dio.post("$apiUrl/user/address/rec_create/", data: formData);
      UserAddressModel userAddress;
      if (res.data != null) {
        UserAddressModel userAddress =
            new UserAddressModel.fromJson(res.data["data"]);
        return userAddress;
      }
      return userAddress;
    } catch (e) {
      print(">>>>>> post address error: ${e.toString()}");
      throw e;
    }
  }

  static Future<dynamic> deleteAddress(int id) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.post("$apiUrl/user/address/delete/$id");
      return res.data;
    } catch (e) {
      print(">>>>>> post address error: ${e.toString()}");
      throw e;
    }
  }

  static Future<dynamic> updateAddress(int addressId, String recName,
      String recAddress, String recNumber) async {
    try {
      var formData = FormData.fromMap({
        'rec_name': recName,
        'rec_address': recAddress,
        'rec_number': recNumber,
      });
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.post("$apiUrl/user/address/update/$addressId",
          data: formData);
      return res.data;
    } catch (e) {
      print(">>>>>> update address error: ${e.toString()}");
      throw e;
    }
  }

  static Future<List<OrderModel>> listOrders(int userId) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/orders/$userId");
      List<OrderModel> data = [];
      if (res.data != null) {
        res.data.forEach((v) {
          data.add(new OrderModel.fromJson(v));
        });
      }
      return data;
    } catch (e) {
      print(">>>>>> fetch orders error: ${e.toString()}");
      throw e;
    }
  }

  static Future<ProfileModel> getMyProfile() async {
    try {
      String userId = appStore.state.authState.userId;

      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/user-accounts/$userId");
      ProfileModel data = ProfileModel.fromJson(res.data);
      return data;
    } catch (e) {
      print(">>>>>> fetch profile error: ${e.toString()}");
      throw e;
    }
  }

  static Future<ProfileModel> getProfileById(id) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/user-accounts/$id");
      ProfileModel data = ProfileModel.fromJson(res.data);
      return data;
    } catch (e) {
      print(">>>>>> fetch profile error: ${e.toString()}");
      throw e;
    }
  }

  static Future<ProfileModel> updateMyProfile(Object body) async {
    try {
      String userId = appStore.state.authState.userId;

      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.patch("$apiUrl/user-accounts/$userId", data: body);
      ProfileModel data = ProfileModel.fromJson(res.data);

      return data;
    } catch (e) {
      print(">>>>>> update profile error: ${e.toString()}");
      throw e;
    }
  }
}
