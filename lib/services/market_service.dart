import 'package:chynar_mobile/config/constants.dart';
import 'package:chynar_mobile/models/market_category_model.dart';
import 'package:chynar_mobile/models/market_model.dart';
import 'package:chynar_mobile/models/product_model.dart';
import 'package:chynar_mobile/models/unit_model.dart';
import 'package:chynar_mobile/utils/dio_util.dart';

class MarketService {
  MarketService._();

  static Future<List<MarketModel>> list() async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/markets");
      List<MarketModel> data = [];
      if (res.data != null) {
        res.data.forEach((v) {
          data.add(new MarketModel.fromJson(v));
        });
      }
      return data;
    } catch (e) {
      print(">>>>>> fetch markets error: ${e.toString()}");
      throw e;
    }
  }

  static Future<List<MarketCategoryModel>> listCategories(int id) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/market/kategoriya/$id");
      List<MarketCategoryModel> data = [];
      if (res.data != null) {
        res.data.forEach((v) {
          data.add(new MarketCategoryModel.fromJson(v));
        });
      }
      return data;
    } catch (e) {
      print(">>>>>> fetch market categories error: ${e.toString()}");
      throw e;
    }
  }

  static Future<List<ProductModel>> listProducts(int id) async {
    try {
      String apiUrl = Constants.apiUrl;
      var dio = createDio();
      var res = await dio.get("$apiUrl/product/market/$id");
      List<ProductModel> data = [];
      if (res.data != null) {
        res.data.forEach((v) {
          data.add(new ProductModel.fromJson(v));
        });
      }
      return data;
    } catch (e) {
      print(">>>>>> fetch products error: ${e.toString()}");
      throw e;
    }
  }

  static Future<List<UnitModel>> listUnits(int id) async {
    try {
      // String apiUrl = Constants.apiUrl;
      // var dio = createDio();
      // var res = await dio.get("$apiUrl/product/market/$id");
      var data = [
        {
          "id": 1,
          "name": "kg",
          "createdAt": "2021-07-12T15:21:50.994Z",
          "updatedAt": "2021-07-12T15:21:50.994Z"
        },
        {
          "id": 12,
          "name": "ştuk",
          "createdAt": "2021-07-30T07:32:49.375Z",
          "updatedAt": "2021-07-30T07:32:49.375Z"
        },
        {
          "id": 2,
          "name": "gram",
          "createdAt": "2021-07-17T08:10:26.500Z",
          "updatedAt": "2021-07-30T07:33:01.351Z"
        }
      ];
      List<UnitModel> units = [];
      if (data != null) {
        data.forEach((v) {
          units.add(new UnitModel.fromJson(v));
        });
      }
      return units;
    } catch (e) {
      print(">>>>>> fetch products error: ${e.toString()}");
      throw e;
    }
  }
}
